<?php
namespace dreamhouse\controllers\price;

use \dreamhouse\application as app;
use \dreamhouse\models\common\adminController as adminController;
use \dreamhouse\models\orm\price as OrmPrice;
use \dreamhouse\models\libs\pagination as pagination;

class main extends adminController
{

    function __before()
    {

        parent::__before();
        $this->view->setPath(app::$device . '/price');
        $this->price = new OrmPrice();
        if (!isset($_SESSION['PRICE_CAT'])) $_SESSION['PRICE_CAT'] = 0;
    }

    function __default($args = false)
    {
        $this->view->topPrice = "active";
        if (isset($_SESSION['PRICE_SEARCH_STR'])) $this->view->searchstr = $_SESSION['PRICE_SEARCH_STR'];
        $this->view->fixed_footer = "am-sticky-footer";
        $this->view->cats = $this->price->getAllCatList();
        $this->view->ccat = $_SESSION['PRICE_CAT'];
        $this->view->setTemplate('index.tpl');
        return $this->view;
    }

    function getContent()
    {

        $pages = new pagination();
        $where = array();
        $search = array();
        if (isset($_GET['query'])) {
            $r = $this->makeTags($_GET['query']);

            $srch = $r['str'];
            if (strlen($srch)) {
                $_SESSION['PRICE_SEARCH_NAME'] = array($srch);
                $_SESSION['PRICE_SEARCH_STR'] = $srch;

            } else {
                $_SESSION['PRICE_SEARCH_NAME'] = NULL;
                $_SESSION['PRICE_SEARCH_STR'] = NULL;

            }
        }
        if ($_SESSION['PRICE_CAT']) {
            $where['price_cat'] = $_SESSION['PRICE_CAT'];
        }

        if (isset($_SESSION['PRICE_SEARCH_STR'])) {

            $totalRec = $this->price->getListCountSearch($where, $_SESSION['PRICE_SEARCH_NAME']);
        } else {

            $totalRec = $this->price->getListCount($where, $search);
        }

        $page = 200;
        $itemsOnPage = 200;

        $pagination = $pages->calculate_pages($totalRec, $itemsOnPage, $page);
        $order = 'price_id ASC';

        $this->view->totalCount = $totalRec;

        $this->view->curl = $_SERVER['REQUEST_URI'];
        $this->view->iop = $itemsOnPage;
        $this->view->pagination = $pagination;
        $this->view->totalRec = $totalRec;

        if (!isset($_SESSION['PRICE_SEARCH_STR']) && ($_SESSION['PRICE_CAT'] < 1)) {
            $records = $this->price->getAllCatList();
            foreach ($records as $k => $v) {

                $where['price_cat'] = $v['price_cat_id'];
                $sub = $this->price->getList($where, $pagination['current'], $itemsOnPage, $search, $order);
                $records[$k]['sub'] = $sub;

            }

            $this->view->records = $records;
            $this->view->setTemplate('page.tpl');
            $html = $this->view->render();

            $this->view->setTemplate('footer.tpl');
            $footer = $this->view->render();

            $res['footer'] = $footer;
            $res['count'] = $totalRec;
            $res['html'] = $html;
            return $res;
        } else if (isset($_SESSION['PRICE_SEARCH_STR'])) {
            $rec = $this->price->getListSearch($where, $pagination['current'], $itemsOnPage, $_SESSION['PRICE_SEARCH_NAME'], $order);
            $this->view->rec = $rec;
            $this->view->setTemplate('page.tpl');
            $html = $this->view->render();

            $this->view->setTemplate('footer.tpl');
            $footer = $this->view->render();

            $res['footer'] = $footer;
            $res['count'] = $totalRec;
            $res['html'] = $html;
            return $res;

        } else if (isset($_SESSION['PRICE_CAT'])) {
            $where['price_cat'] = $_SESSION['PRICE_CAT'];
            $sub = $this->price->getListCat($where, $pagination['current'], $itemsOnPage, $search, $order);
            $this->view->rec = $sub;
            $this->view->setTemplate('page.tpl');
            $html = $this->view->render();

            $this->view->setTemplate('footer.tpl');
            $footer = $this->view->render();

            $res['footer'] = $footer;
            $res['count'] = $totalRec;
            $res['html'] = $html;
            return $res;

        }
    }

    function changeOptions()
    {

        $_SESSION['PRICE_CAT'] = $_POST['cat'];
        $res['status'] = true;
        return $res;

    }

    function editCat(){
        $id = $_GET['id'];
        $this->view->price = $this->price->getByID($id);
        $this->view->cat = $this->price->getAllCatList();
        $this->view->unit = $this->price->getAllUnitsList();
        $this->view->setTemplate('edit.tpl');
        $html = $this->view->render();
        $res['html'] = $html;
        $res['status'] = true;
        return $res;
    }
    function doEditCat(){
        $name = app::strings_clear($_POST['price_name']);
        $id=$_POST['price_id'];

        $upd = array(
            "price_name" => $name,
            "price_price"=>$_POST['price'],
            "price_cat"=>$_POST['price_cat_id'],
            "price_unit_id"=>$_POST['price_unit_id'],
        );
        $this->price->update($id, $upd);
        $res['status'] = true;
        return $res;
    }

    function delete($id=0){

        if(!$id)$id=$_POST['id'];

        $upd['price_trash'] = 1;
        $this->price->update($id,$upd);
        $res['status'] = true;
        return $res;

    }

    function add(){
        $this->view->cat = $this->price->getAllCatList();
        $this->view->unit = $this->price->getAllUnitsList();
        $this->view->setTemplate('add.tpl');
        $html = $this->view->render();
        $res['html'] = $html;
        $res['status'] = true;
        return $res;
    }

    function doAdd(){
        $name = app::strings_clear($_POST['price_name']);
        $add= array(
            "price_name" => $name,
            "price_price"=>$_POST['price'],
            "price_cat"=>$_POST['price_cat_id'],
            "price_unit_id"=>$_POST['price_unit_id'],
        );
        $this->price->add($add);
        $res['status'] = true;
        return $res;
    }

//    function add($args = false) {
//        $rates = array();
//        $where['expcat_parent_id'] = 0;
//        $cats = $this->expcat->getAllList($where);
//
//        $allcurrencies = $this->misc->getCurrencies();
//
//        foreach($allcurrencies as $k=>$v){
//            $ra = $this->misc->getRateByCurrency($v['currency_iso'],$_SESSION['account']['user_currency']);
//            $rates[$k]['curr'] = $v['currency_iso'];
//            $rates[$k]['val'] = $ra['exr_rate'];
//        }
//        $this->view->currencies = $allcurrencies;
//
//        foreach($cats as $k=>$v){
//
//            $selSubCat['expcat_parent_id'] = $v['expcat_id'];
//            $sub = $this->expcat->getAllList($selSubCat);
//            $cats[$k]['sub']  = $sub;
//
//        }
//
//        $wherec['expcat_parent_id'] = 0;
//        $wherec['expcat_default']=0;
//
//        $cat = $this->expcat->getAllList($wherec);
//
//
//        $this->view->paymethod = $this->trans->getPaymentMethodsList();
//
//        $this->view->rates = $rates;
//        $this->view->cats = $cats;
//        $this->view->cat = $cat;
//        $this->view->setTemplate('add.tpl');
//        return $this->view;
//    }
//
//    function doAdd(){
//
//        $add = array();
//        $am = trim($_POST['amount']);
//        $base = trim($_POST['base']);
//        $vendor = app::strings_clear($_POST['vendor']);
//        $str = app::strings_clear($_POST['name']);
//        list($catid,$parentcatid) = explode("@", $_POST['cat']);
//        $date = \date("Y-m-d",strtotime($_POST['date']));
//        if(!strlen($am))$am=0;
//        if(!strlen($base))$base=0;
//        if(!strlen($_POST['vendor_id']))$_POST['vendor_id']=0;
//        //app::trace($_POST['cat']);
//        $add= array(
//            "exp_user_id" => $_SESSION['account']['user_id'],
//            "exp_date" => $date,
//            "exp_desc" => $str,
//            "exp_vendor" => $vendor,
//            "exp_method" => $_POST['method'],
//            "exp_vendor_id" => $_POST['vendor_id'],
//            "exp_tax1_cash" => $_POST['tax'],
//            "exp_category" => $catid,
//            "exp_category_parent" => $parentcatid,
//            "exp_pic" => $_POST['pic'],
//            "exp_amount" => $am,
//            "exp_amount_base" => $_POST['base'],
//            "exp_exchange_rate" => $_POST['exchange_rate'],
//            "exp_currency" => $_POST['currency'],
//            //"exp_id" => getLastID()
//
//        );
//        $id = $this->exp->add($add);
//
//        if(!$_POST['vendor_id']){
//
//            $addvendor['vendor_user_id'] = $_SESSION['account']['user_id'];
//            $addvendor['vendor_name'] = $vendor;
//
//            $this->vendor->add($addvendor);
//        }
//
//
//        if(isset($_POST['el_desc'])){
//
//            foreach($_POST['el_desc'] as $k=>$v){
//                $str = app::strings_clear($v);
//                if(strlen($str)){
//                    $addline = array(
//
//                        "el_user_id" => $_SESSION['account']['user_id'],
//                        "el_expense_id" => $id,
//                        "el_desc" => $str,
//                        "el_amount" => $_POST['el_amount'][$k]
//                    );
//
//                    $this->exp->addLine($addline);
//
//                }
//
//            }
//
//        }
//
//        $addt= array(
//            "t_user_id"	=> $_SESSION['account']['user_id'],
//            "t_exp_id"	=> $id,
//            "t_method"	=> $_POST['method'],
//            "t_date"	=> $date,
//            "t_amount"	=> $am,
//            "t_notes"		=> $str,
//            "t_exchange_rate"	=> $_POST['exchange_rate'],
//            "t_amount_base" => $_POST['base'],
//            "t_currency" => $_POST['currency'],
//            "t_tags" => $am . " " . $str,
//        );
//
//        $this->trans->add($addt);
//
//        $res['status'] = true;
//        return $res;
//
//    }
//
//    function changeCat(){
//        list($cat,$catparent)=explode("@",$_POST['cat']);
//        $_SESSION['EXP_CATEGORY'] = $cat;
//        $_SESSION['EXP_CATEGORY_PARENT'] = $catparent;
//        $res['status'] = true;
//        return $res;
//    }
//
//    function changeDateRange(){
//
//        $_SESSION['EXP_STARTDATE'] = $_POST['start'];
//        $_SESSION['EXP_ENDDATE'] = $_POST['end'];
//
//        $res['s'] = $_SESSION['PRICE_SEARCH_STR'];
//        $res['status'] = true;
//        return $res;
//    }
//
//    function getViewBox(){
//
//        $id = $_GET['id'];
//        $exp = $this->exp->getByID($id);
//        $this->view->id = $id;
//        $this->view->exp = $exp;
//        $this->view->setTemplate('view.tpl');
//        $html = $this->view->render();
//        $res['id']= $id;
//        $res['html'] = $html;
//        $res['status'] = true;
//
//        return $res;
//
//    }
//
//    function edit() {
//        $rates = array();
//        if(!isset($this->args[0])){
//            $this->view->setPath(app::$device.'/common');
//            $this->view->setTemplate('404.tpl');
//            return $this->view;
//        }
//        $id = $this->args[0];
//        $exp = $this->exp->getByID($id);
//
//        if(!$exp){
//            $this->view->setPath(app::$device.'/common');
//            $this->view->setTemplate('404.tpl');
//            return $this->view;
//        }
//        $where['expcat_parent_id'] = 0;
//        $cats = $this->expcat->getAllList($where);
//
//        $allcurrencies = $this->misc->getCurrencies();
//
//        foreach($allcurrencies as $k=>$v){
//            $ra = $this->misc->getRateByCurrency($v['currency_iso'],$_SESSION['account']['user_currency']);
//            $rates[$k]['curr'] = $v['currency_iso'];
//            $rates[$k]['val'] = $ra['exr_rate'];
//        }
//        $this->view->currencies = $allcurrencies;
//
//        foreach($cats as $k=>$v){
//
//            $selSubCat['expcat_parent_id'] = $v['expcat_id'];
//            $sub = $this->expcat->getAllList($selSubCat);
//            $cats[$k]['sub']  = $sub;
//
//        }
//
//        $this->view->explines = $this->exp->getLineList($id);
//
//        $this->view->paymethod = $this->trans->getPaymentMethodsList();
//        $this->view->exp = $exp;
//        $this->view->rates = $rates;
//        $this->view->cats = $cats;
//        $this->view->setTemplate('edit.tpl');
//        return $this->view;
//    }
//
//    function doUpdate(){
//
//        //$upd = array();
//        $id = $_POST['exp_id'];
//        //app::trace($id);
//        $am = trim($_POST['amount']);
//        $base = trim($_POST['base']);
//        $vendor = app::strings_clear($_POST['vendor']);
//        $str = app::strings_clear($_POST['name']);
//        list($catid,$parentcatid) = explode("@", $_POST['cat']);
//        $date = \date("Y-m-d",strtotime($_POST['date']));
//        if(!strlen($am))$am=0;
//        if(!strlen($base))$base=0;
//        if(!strlen($_POST['vendor_id']))$_POST['vendor_id']=0;
//
//        $upd= array(
//            "exp_user_id" => $_SESSION['account']['user_id'],
//            "exp_date" => $date,
//            "exp_desc" => $str,
//            "exp_vendor" => $vendor,
//            "exp_method" => $_POST['method'],
//            "exp_vendor_id" => $_POST['vendor_id'],
//            "exp_tax1_cash" => $_POST['tax'],
//            "exp_category" => $catid,
//            "exp_category_parent" => $parentcatid,
//            "exp_pic" => $_POST['pic'],
//            "exp_amount" => $am,
//            "exp_amount_base" => $_POST['base'],
//            "exp_exchange_rate" => $_POST['exchange_rate'],
//            "exp_currency" => $_POST['currency']
//        );
//
//        $this->exp->update($id, $upd);
//
//        if(!$_POST['vendor_id']){
//
//            $addvendor['vendor_user_id'] = $_SESSION['account']['user_id'];
//            $addvendor['vendor_name'] = $vendor;
//
//            $this->vendor->add($addvendor);
//        }
//
//        $updt= array(
//            //"t_exp_id"	=> $id,
//            "t_user_id"	=> $_SESSION['account']['user_id'],
//            "t_method"	=> $_POST['method'],
//            "t_date"	=> $date,
//            "t_amount"	=> $am,
//            "t_notes"		=> $str,
//            "t_exchange_rate"	=> $_POST['exchange_rate'],
//            "t_amount_base" => $_POST['base'],
//            "t_currency" => $_POST['currency'],
//            "t_tags" => $am . " " . $str,
//        );
//
//
//        $this->trans->update($id, $updt);
//
//        $this->exp->delOldLines($id);
//
//        if(isset($_POST['el_desc'])){
//
//            foreach($_POST['el_desc'] as $k=>$v){
//                $str = app::strings_clear($v);
//                if(strlen($str)){
//                    $addline = array(
//
//                        "el_user_id" => $_SESSION['account']['user_id'],
//                        "el_expense_id" => $id,
//                        "el_desc" => $str,
//                        "el_amount" => $_POST['el_amount'][$k]
//                    );
//
//                    $this->exp->addLine($addline);
//
//                }
//
//            }
//
//        }
//
//
//        $res['status'] = true;
//        return $res;
//
//    }
//
//    function changePage(){
//
//        $_SESSION['EXP_PAGE'] = $_POST['p'];
//
//        $res['status'] = true;
//        return $res;
//
//    }
//
//    function delete($id=0){
//
//        if(!$id)$id=$_POST['id'];
//
//        $upd['exp_trash'] = 1;
//        $this->exp->update($id,$upd);
//
//        $updTrans['t_trash'] =1;
//        $sel['t_exp_id'] =$id;
//        $this->trans->updateMass($sel,$updTrans);
//
//        $res['status'] = true;
//        return $res;
//
//    }
//
//    function deleteBundle(){
//
//
//        if(isset($_POST['row']) && count($_POST['row'])){
//
//            foreach($_POST['row'] as $val){
//                $this->delete($val);
//            }
//
//            $res['status']=true;
//            return $res;
//        }else{
//            $res['title']="Operation Failed";
//            $res['status']=false;
//            $res['msg']="No rows selected. Please select at least one template.";
//            return $res;
//        }
//    }
//
//    function doAddCat(){
//
//        $name = app::strings_clear($_POST['cat_name']);
//
//        $add = array(
//            "expcat_user_id" => $_SESSION['account']['user_id'],
//            "expcat_name" =>  $name,
//            "expcat_parent_id" => $_POST['parent'],
//        );
//        $id = $this->expcat->add($add);
//        $selectbox='';
//        //$where['expcat_parent_id'] = 0;
//        $cats = $this->expcat->getAllList();
//        foreach($cats as $v){
//
//            if($v['expcat_id']==$id){
//                $selectbox.='<option selected value="'.$v['expcat_id'].'@'.$v['expcat_parent_id'].'">'.$v['expcat_name'].'</option>';
//
//            }else{
//                $selectbox.='<option value="'.$v['expcat_id'].'@'.$v['expcat_parent_id'].'">'.$v['expcat_name'].'</option>';
//
//                foreach($cats as $k=>$v){
//
//                    $selSubCat['expcat_parent_id'] = $v['expcat_id'];
//                    $sub = $this->expcat->getAllList($selSubCat);
//                    $cats[$k]['sub']  = $sub;
//
//                }
//            }
//        }
//
////        if($cats){
////            foreach($cats as $v) {
////                $selectbox .= '<option selected value="' . $v['expcat_id'] . '@' . $v['expcat_parent_id'] . '">' . $v['expcat_name'] . '</option>';
////            }
////        }
////         if($cats){
////                $where['expcat_parent_id'] = 0;
////                $cats = $this->expcat->getAllList($where);
////
////                foreach($cats as $k=>$v){
////
////                  $selSubCat['expcat_parent_id'] = $v['expcat_id'];
////                     $sub = $this->expcat->getAllList($selSubCat);
////                 $cats[$k]['sub']  = $sub;
////
////             }
////        }
////        $where['expcat_parent_id'] = 0;
////        $cats = $this->expcat->getAllList($where);
////
////        foreach($cats as $k=>$v){
////
////            $selSubCat['expcat_parent_id'] = $v['expcat_id'];
////            $sub = $this->expcat->getAllList($selSubCat);
////            $cats[$k]['sub']  = $sub;
////
////        }
////        $this->view->cats = $cats;
////        $res['html'] = $cats;
//        if($_POST['exp_id']){
//            $res['idedit']=$_POST['exp_id'];
//            $res['html'] = $selectbox;
//            $res['id'] = $id;
//            $res['status'] = true;
//            return $res;
//        }else{
//            $res['html'] = $selectbox;
//            $res['id'] = $id;
//            $res['status'] = true;
//            return $res;
//
//        }
//
//
//    }
//
//    function getNewLine(){
//
//        $this->view->id = uniqid();
//        $this->view->setTemplate('newline.tpl');
//        $res['html'] = $this->view->render();
//        $res['status'] = true;
//        $res['id'] = $this->view->id;
//        return $res;
//
//
//    }

}
