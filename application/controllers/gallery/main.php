<?php
namespace dreamhouse\controllers\gallery;

use \dreamhouse\application as app;
use \dreamhouse\models\common\adminController as adminController;
use \dreamhouse\models\orm\gallery as OrmGallery;
use \dreamhouse\models\orm\users as OrmAccounts;
use \dreamhouse\models\libs\pagination as pagination;

class main extends adminController
{

    function __before(){

        parent::__before();
        $this->view->setPath(app::$device . '/gallery');
        $this->gallery = new OrmGallery();
        $this->acc = new OrmAccounts();
    }

    function __default($args = false){
        $this->view->topGallery = "active";
        $this->view->fixed_footer = "am-sticky-footer";
        $this->view->setTemplate('index.tpl');
        return $this->view;
    }

    function getContent()
    {

        $pages = new pagination();
        $where = array();
        $search = array();
        if (isset($_GET['query'])) {
            $r = $this->makeTags($_GET['query']);

            $srch = $r['str'];
            if (strlen($srch)) {
                $_SESSION['GALLERY_SEARCH_NAME'] = array($srch);
                $_SESSION['GALLERY_SEARCH_STR'] = $srch;

            } else {
                $_SESSION['GALLERY_SEARCH_NAME'] = NULL;
                $_SESSION['GALLERY_SEARCH_STR'] = NULL;

            }
        }

        if (isset($_SESSION['GALLERY_SEARCH_STR'])) {

            $totalRec = $this->gallery->getListCountSearch($where, $_SESSION['GALLERY_SEARCH_NAME']);
        } else {

            $totalRec = $this->gallery->getListCount($where, $search);
        }

        $page = isset($_GET['p']) && $_GET['p'] ? \abs((int)$_GET['p']) : 1;
        $itemsOnPage =$_SESSION["iop"];

        $pagination = $pages->calculate_pages($totalRec, $itemsOnPage, $page);
        $order = 'gallery_id ASC';

        $this->view->totalCount = $totalRec;

        $this->view->curl = $_SERVER['REQUEST_URI'];
        $this->view->iop = $itemsOnPage;
        $this->view->pagination = $pagination;
        $this->view->totalRec = $totalRec;

        if (isset($_SESSION['GALLERY_SEARCH_STR'])) {

            $this->view->records = $this->gallery->getListSearch($where, $pagination['current'], $itemsOnPage, $_SESSION['GALLERY_SEARCH_TITLE'], $order);
        } else {
            $this->view->records = $this->gallery->getList($where, $pagination['current'], $itemsOnPage, $search, $order);
        }

        $this->view->setTemplate('page.tpl');
        $html = $this->view->render();

        $this->view->setTemplate('footer.tpl');
        $footer = $this->view->render();

//        $res['returl'] = $_SESSION['GALLERY_RET_URL'];
        $res['footer'] = $footer;
        $res['html'] = $html;
        $res['count'] = $totalRec;
        $res['pag'] = $pagination;
        return $res;

    }

    function add()
    {
        $this->view->setTemplate('add.tpl');
        return $this->view;
    }

    function edit(){

        $id = $this->args[0];
        $this->view->ihost =  app::$proto.app::$config->site['userhost'] ;
        $this->view->gallery = $this->gallery->getByID($id);
        $this->view->gallery_pic = $this->gallery->getGallerypic($id);
        $this->view->setTemplate('edit.tpl');
        return $this->view;
    }

    function doAdd()
    {

        $title = app::strings_clear($_POST['gallery_title']);
        $desc = $_POST['gallery_desc'];
        $add = array(
            "gallery_title" => $title,
            "gallery_desc" => $desc,
            "gallery_thumbnail" => $_POST['fcover'],
        );

        $id=$this->gallery->add($add);
        if(isset($_POST['fpic'])){
            foreach($_POST['fpic'] as $k=>$v){

                $addp["gallery_pic_pic"] = $v;
                $this->gallery->addGallerypic($addp,$id);
                $addp=array();

            }

        }
        //app::trace($addp);
        $res['status'] = true;
        return $res;
    }

    function doUpdate(){
        $title = app::strings_clear($_POST['gallery_title']);
        $desc = $_POST['gallery_desc'];
        $id=$_POST['gallery_id'];

        $upd = array(
            "gallery_title" => $title,
            "gallery_desc" => $desc,
            "gallery_thumbnail" => $_POST['fcover'],
        );

        $this->gallery->update($id, $upd);

        $this->gallery->delGallerypic($id);

        if(isset($_POST['fpic'])){
            foreach($_POST['fpic'] as $k=>$v){


                $addp["gallery_pic_pic"] = $v;
                $this->gallery->addGallerypic($addp,$id);
                $addp=array();

            }

        }

        $res['status'] = true;

        return $res;
    }

    function del($id=0){
        if(!$id)$id=$_POST['id'];
        $upd['gallery_trash'] = 1;
        $this->gallery->updateMass($id,$upd);
        $res['status'] = true;
        return $res;
    }
//
    function deleteBundle(){


        if(isset($_POST['row']) && count($_POST['row'])){
            // app::trace($_POST);
            foreach($_POST['row'] as $val){
                $this->del($val);
            }

            $res['status']=true;
            return $res;
        }else{
            $res['title']="Operation Failed";
            $res['status']=false;
            $res['msg']="No rows selected. Please select at least one template.";
            return $res;
        }
    }
}