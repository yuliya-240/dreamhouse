<?php      
namespace dreamhouse\controllers;

use \dreamhouse\application as app;
use \dreamhouse\models\common\adminController as adminController;

class main extends adminController {
    
    function __before() {

    	parent::__before();
        $this->view->setPath(app::$device.'/dashboard');
		$this->view->topHome = "active";

	}   
     
    function __default($args = false) {
        $this->view->setTemplate('index.tpl');
        return $this->view;
    }


    
    function logout(){

	    $this->session->logout();
	    \jet\redirect('/',true);
    }
    
    
    function __format() {
        return array(
         
        );
    } 
    
}
