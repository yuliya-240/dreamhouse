<?php      
namespace dreamhouse\controllers\login;

use \dreamhouse\application as app;
use \dreamhouse\models\common\adminController as adminController;
use \dreamhouse\models\orm\users as OrmUsers;

class main extends adminController {
    
    function __before() {

    	parent::__before();
    	if ($this->session->isLogged() ) \jet\redirect('/',true);
    	//app::trace($this->session->isLogged());
        $this->acc = new OrmUsers();
        $this->view = new \jet\twig();
        $this->view->setPath(app::$device.'/login');
        $this->skin=app::$device;
        $this->view->skin = $this->skin;
        $this->view->hostname = app::$proto.app::$config->site['host'];


	}   
     
    function __default($args = false) {
        $this->view->setTemplate('index.tpl');
        return $this->view;
    }


    function doLogin($email = null,$pass = null,$hash = false){

        if($_SERVER['REQUEST_METHOD'] != 'POST'){

            return false;
        }

        if(!$email && $_SERVER['REQUEST_METHOD'] == 'POST'){

            $email=\strtolower(\trim($_POST['email']));;
            $pass=$_POST['password'];
            $hash = false;
        }

        try{
            $account=$this->session->login($email,$pass,$hash);
        }catch (\Exception $e) {
            $e->getMessage();
            $res['status']=false;
            $res['msg']="ERROR";
            $res['title']=$e->getMessage();;
            return $res;


        }

        if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['remember']) && $_POST['remember'] ){

            $this->session->enableAutoLogin($email, $account['user_password']);
        }

        $res['url']='/';
        $res['status']=true;
        return $res;

    }
    
    function logout(){

	    $this->session->logout();
	    \jet\redirect('/login',true);
    }
    
    
    
}
