<?php
namespace dreamhouse\models\orm;

use
    \dreamhouse\application as app;
use \jet\db\qb as qb;

class price extends \dreamhouse\models\common\model {

    function add($add) {

        $this->db->q(qb::_table('price')->insert($add));
        return $this->db->getLastID();

    }

    function update($id=0, $data = array()) {
        $where = array(
            'price_id' => $id
        );

        $this->db->q(qb::_table('price')->where($where)->update($data));
        return true;
    }

    function updateMass($where=array(), $data = array()) {


        $this->db->q(qb::_table('price')->where($where)->update($data));
        return true;
    }

//    function getList($where=array(),$page=1,$count=1, $search = array(),$order='price_name ASC') {
//
//        $where['price_trash']=0;
//        if($page>0)$page--;
//        $select='*';
//        $offset = \intval($page*$count);
//        $collection = qb::_table('price');
//        $collection->leftjoin('units', 'units.unit_id', 'price.price_unit_id');
//        $collection->leftjoin('price_cat', 'price_cat.price_cat_id', 'price.price_cat');
//        return $this->db->q($collection->where($where)->Search($search)->OrderBy($order)->Limit($offset,$count)->select($select));
//
//    }
    function getList($where=array(),$page=1,$count=1, $search = array(),$order='price_name ASC'){
        $where['price_trash']=0;
        if($page>0)$page--;
        $offset = \intval($page*$count);
        $select='*';
        $collection = qb::_table('price');
        $collection->leftjoin('units', 'units.unit_id', 'price.price_unit_id');
        $collection->leftjoin('price_cat', 'price_cat.price_cat_id', 'price.price_cat');
        return $this->db->q($collection->where($where)->select($select));


    }

    function getListCount($where=array(),$search = array()) {
        $where['price_trash']=0;
        $select = '*';
        $collection = qb::_table('price');
        return $this->db->q1($collection->where($where)->Search($search)->count('*'));

    }

    function getListSearch($where=array(),$page=1,$count=1, $search = array(),$order="price_name ASC") {

        $where['price_trash']=0;
        if($page>0)$page--;
        $offset = \intval($page*$count);

        $add="";
        foreach($search as $k=>$tag){
            if($k>0)$add.=" + ";
            $add.= "(price_name LIKE '".$tag."')";
            $searchnew['price_name'][]=$tag;
        }
        $ss = "(".$add.") as hits";

        $select = '*,'.$ss;
        $collection = qb::_table('price');
        $collection->leftjoin('units', 'units.unit_id', 'price.price_unit_id');
        //$collection->leftjoin('price_cat', 'price_cat.price_cat_id', 'price.price_cat');

        return $this->db->q($collection->where($where)->Search($searchnew)->OrderBy($order)->Limit($offset,$count)->select($select));

    }
    function getListCat($where=array(),$page=1,$count=1, $search = array(),$order="price_id ASC") {

        $where['price_trash']=0;
        if($page>0)$page--;
        $offset = \intval($page*$count);

        $select = '*';
        $collection = qb::_table('price');
        $collection->leftjoin('units', 'units.unit_id', 'price.price_unit_id');
        $collection->leftjoin('price_cat', 'price_cat.price_cat_id', 'price.price_cat');

        return $this->db->q($collection->where($where)->OrderBy($order)->Limit($offset,$count)->select($select));

    }

    function getListCountSearch($where=array(),$search = array()) {
        $where['price_trash']=0;
        $add="";
        foreach($search as $k=>$tag){
            if($k>0)$add.=" + ";
            $add.= "(price_name LIKE '".$tag."')";
            $searchnew['price_name'][]=$tag;
        }
        $ss = "(".$add.") as hits";

        $collection = qb::_table('price');

        $count =  $this->db->q($collection->where($where)->Search($searchnew)->count('*'));

        return count($count);
    }

    function getByID($id=0){

        $where['price_id'] = $id;
        $select = '*';
        $collection = qb::_table('price');
        $collection->leftjoin('units', 'units.unit_id', 'price.price_unit_id');
        $collection->leftjoin('price_cat', 'price_cat.price_cat_id', 'price.price_cat');
        return $this->db->q_($collection->where($where)->select($select));


    }

    function getAllCatList($where=array()){
        $select = '*';
        $collection = qb::_table('price_cat');
        return $this->db->q($collection->where($where)->select('*'));

    }
    function getAllUnitsList($where=array()){
        $select = '*';
        $collection = qb::_table('units');
        return $this->db->q($collection->where($where)->select('*'));

    }


}