<?php
namespace dreamhouse\models\orm;

use \dreamhouse\application as app;
use \jet\db\qb as qb;


class gallery extends \dreamhouse\models\common\model
{

    function add($add)
    {
        $this->db->q(qb::_table('gallery')->insert($add));
        return $this->db->getLastID();
    }

    function update($id = 0, $data = array())
    {
        $w['gallery_id'] = $id;
        $this->db->q(qb::_table('gallery')->where($w)->update($data));
        return true;
    }

    function updateMass($id=0, $data = array()) {
        $where = array(
            'gallery_id' => $id
        );

        $this->db->q(qb::_table('gallery')->where($where)->update($data));
        return true;
    }


    function delete($id = 0)
    {
        $cArr['gallery_trash'] = 1;
        $this->update($id, $cArr);
    }

    function getList($where = array(), $page = 1, $count = 1, $search = array(), $order = 'gallery_id DESC')
    {
        $where['gallery_trash'] = 0;
        if ($page > 0) $page--;
        $offset = \intval($page * $count);
        $select = '*';
        $collection = qb::_table('gallery');
       // $collection->leftjoin('users', 'users.user_id', 'gallery.gallery_user_id');

        return $this->db->q($collection->where($where)->Search($search)->OrderBy($order)->Limit($offset, $count)->select($select));
    }

    function getAllList($id=0)
    {
        $where['gallery_trash'] = 0;
        $where['gallery_user_id'] = $id;
        $select = '*';
        $collection = qb::_table('gallery');

        return $this->db->q($collection->where($where)->select($select));
    }

    function getListCount($where = array(), $search = array())
    {
        $where['gallery_trash'] = 0;
        $select = '*';
        $collection = qb::_table('gallery');
        return $this->db->q1($collection->where($where)->Search($search)->count('*'));
    }

    function getListSearch($where = array(), $page = 1, $count = 1, $search = array(), $order = "gallery_id ASC")
    {

        $where['gallery_trash'] = 0;
        $where['gallery_user_id'] = $_SESSION['account']['user_id'];
        if ($page > 0) $page--;
        $offset = \intval($page * $count);

        $add = "";
        foreach ($search as $k => $tag) {
            if ($k > 0) $add .= " + ";
            $add .= "(gallery_title LIKE '" . $tag . "')";
            $searchnew['gallery_title'][] = $tag;
        }
        $ss = "(" . $add . ") as hits";

        $select = '*,' . $ss;
        $collection = qb::_table('gallery');
        return $this->db->q($collection->where($where)->Search($searchnew)->OrderBy($order)->GroupBy('gallery_id')->Limit($offset, $count)->select($select));
    }

    function getListCountSearch($where = array(), $search = array())
    {
        $where['gallery_trash'] = 0;
        $where['gallery_user_id'] = $_SESSION['account']['user_id'];
        $add = "";
        foreach ($search as $k => $tag) {
            if ($k > 0) $add .= " + ";
            $add .= "(gallery_title LIKE '" . $tag . "')";
            $searchnew['gallery_title'][] = $tag;
        }
        $ss = "(" . $add . ") as hits";

        $collection = qb::_table('gallery');

        $count = $this->db->q($collection->where($where)->Search($searchnew)->GroupBy('gallery_id')->count('*'));
        return count($count);
    }


    function getByID($id = 0)
    {
        $where['gallery_id'] = $id;
        $select = '*';
        $collection = qb::_table('gallery');
       // $collection->leftjoin('gallerypic', 'gallerypic.gallerypic_pid', 'gallery.gallery_id');
        return $this->db->q_($collection->where($where)->select($select));

    }

// gallery pic

    function addGallerypic($addp=array(),$id){

//        $addp['portfoliopic_user_id'] = $_SESSION['account']['user_id'];
        $addp['gallery_pic_gallery_id'] = $id;

        $this->db->q(qb::_table('gallery_pic')->insert($addp));
        return $this->db->getLastID();
    }

    function getGallerypic($id=0){

        $where['gallery_pic_gallery_id']=$id;
        $select = '*';
        $collection = qb::_table('gallery_pic');
        return $this->db->q($collection->where($where)->select('*'));
    }

    function delGallerypic($id=0){

        $where['gallery_pic_gallery_id'] = $id;
        $collection = qb::_table('gallery_pic');
        $this->db->q($collection->where($where)->delete());
        return false;

    }
}
