{% if records %}
    <div class="table-responsive">
        <form id="flist">
            <table class="table  table-hover">
                <thead>
                <tr>
                    <th>Наименование</th>
                    <th>Ед.изм.</th>
                    <th>Цена(грн.)</th>
                    {% if acs.user_id %}
                        <th>Действие</th>
                    {% endif %}
                </tr>
                </thead>
                <tbody>

                {% for r in records %}
                    <tr>
                        <td colspan="3" style="font-weight: 900; background-color: #ff614e;"
                            data-id="{{ r.price_cat_id }}">{{ r.price_cat_name }}</td>

                    </tr>
                    {% for s in r.sub %}
                        <tr>
                            <td data-id="{{ s.price_id }}">{{ s.price_name }}</td>
                            <td data-id="{{ s.price_id }}">{{ s.unit_name }}</td>
                            <td data-id="{{ s.price_id }}">{{ s.price_price }}</td>

                            {% if acs.user_id %}
                                <td>
                                    <a href="#" id="edit"  data-id="{{ s.price_id }}" class="btn-small">Изменить</a>
                                    <a href="#" id="del"  data-id="{{ s.price_id }}" class="btn-small">Удалить</a>
                                </td>
                            {% endif %}
                        </tr>
                    {% endfor %}


                {% endfor %}
                </tbody>

            </table>


        </form>
    </div>
{% elseif rec %}
    <div class="table-responsive">
        <form id="flist">
            <table class="table  table-hover">
                <thead>
                <tr>
                    <th>Наименование</th>
                    <th>Ед.изм.</th>
                    <th>Цена</th>

                    {% if acs.user_id %}
                        <th>Действие</th>
                    {% endif %}
                </tr>
                </thead>
                <tbody>

                {% for r in rec %}

                    <tr>
                        <td data-id="{{ r.price_id }}">{{ r.price_name }}</td>
                        <td data-id="{{ r.price_id }}">{{ r.unit_name }}</td>
                        <td data-id="{{ r.price_id }}">{{ r.price_price }}</td>

                        {% if acs.user_id %}
                            <td>
                                <a href="#" id="edit"  data-id="{{ r.price_id }}" class="btn-small">Изменить</a>
                                <a href="#" id="del"  data-id="{{ r.price_id }}" class="btn-small">Удалить</a>
                            </td>
                        {% endif %}
                    </tr>


                {% endfor %}
                </tbody>

            </table>


        </form>
    </div>
{% else %}
    <div class="text-center">
        Нет Контента
    </div>

{% endif %}
