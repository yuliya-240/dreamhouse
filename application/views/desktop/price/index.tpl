{% extends skin~"/common/root.tpl" %}
{%block preheadcss%}
    <link rel="stylesheet" type="text/css" href="/assets/vendor/select2/css/select2.css"/>
{%endblock%}
{% block content %}

    <div class="container" style="padding: inherit">
            <div class="row">
                <div class="col-sm-8">
                    <form id="fsearch"  class="form-horizontal group-border-dashed">
                        <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon " style="background-color:#ff614e; color:#fff; " ><span class="fa fa-search" style="font-size: 24px;"></span></span>
                                    <input class="form-control" id="s" placeholder="Поиск" {%if searchstr%}value="{{searchstr}}"{%endif%} type="text" style="border-width:1px;" >
                                </div>
                        </div>
                    </form>

                </div>

                <div class="col-sm-4 form-group">
                    <div class="form-group">
                        <div class="input-group">
                    <select id="cat" name="cat" class="select2">
                        <option value="0">Все категории</option>
                        {%for c in cats%}
                            <option {%if ccat==c.price_cat_id%}selected{%endif%} value="{{c.price_cat_id}}">{{c.price_cat_name}}</option>
                        {%endfor%}
                    </select>
                </div>
                </div>
                </div>

            </div>
            {% if acs.user_id %}
            <div class="row">
                <button id="add">Добавить услугу</button>
            </div>
        {% endif %}
            <div class="row">
                <div class="col-sm-12">

                    <form id="flist" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
                        <div class="panel panel-default panel-borders">

                            <div class="panel-body">

                                <div id="d-content">

                                </div>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    <div id="new-cat-box" tabindex="-1" role="dialog" class="modal modal-colored-header fade" >
        <div style="width: 100%;" class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><i class="icon s7-close"></i></button>
                    <h3 class="modal-title">Редактирование Записи</h3>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <form id="fdata" class="form-horizontal" role="form">

                                <div class="form-group edit-cat">

                                </div>
                            </form>

                        </div>
                    </div>
                </div>

                <div class="modal-footer" >

                    <button type="submit" id="doSave"   class="btn btn-primary doShare btn-md ">Сохранить</button>
                    <button type="submit" id="doEdit" style="display: none"  class="btn btn-primary doShare btn-md ">Изменить</button>

                    <button type="button"  class="btn btn-default btn-md  pull-left " data-dismiss="modal">Отменить</button>

                </div>



            </div>
        </div>
    </div>


{% endblock %}
{%block footer%}
    <div id="megafooter"></div>

{%endblock%}


{% block js %}
    <script src="/js/price.js" type="text/javascript"></script>

{% endblock %}
