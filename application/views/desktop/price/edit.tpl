<input type="hidden" name="price_id" value="{{ price.price_id }}">
<label class="col-sm-3 control-label no-padding-right ">Название</label>
<div class="col-sm-9">
    <input type="text" id="price_name" name="price_name" class="form-control" value="{{ price.price_name }}">
</div>

<label class="col-sm-3 control-label no-padding-right ">Категория</label>
<div class="col-sm-9">
    <select name="price_cat_id" class="select2">
        {%for c in cat%}
            <option {% if price.price_cat_id==c.price_cat_id  %}selected{% endif %} value="{{c.price_cat_id}}">{{c.price_cat_name}}</option>
        {%endfor%}
    </select>
    {#<input type="text" id="cat_name" name="cat_name" class="form-control" value="{{ price.price_cat }}">#}
</div>
<label class="col-sm-3 control-label no-padding-right ">Единица измерения</label>
<div class="col-sm-9">
    <select name="price_unit_id" class="select2">
    {%for c in unit%}
        <option {% if price.price_unit_id==c.unit_id  %}selected{% endif %} value="{{c.unit_id}}">{{c.unit_name }}</option>
    {%endfor%}
    </select>
    {#<input type="text" id="cat_name" name="cat_name" class="form-control" value="{{ price.price_price }}">#}
</div>
<label class="col-sm-3 control-label no-padding-right ">Цена</label>
<div class="col-sm-9">
    <input type="text" id="price" name="price" class="form-control" value="{{ price.price_price }}">
</div>