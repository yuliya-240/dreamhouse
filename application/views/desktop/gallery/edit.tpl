{% extends skin~"/common/root.tpl" %}
{% block css %}
    <link rel="stylesheet" type="text/css" href="/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/vendor/sweetalert/sweetalert.min.css">
    <link rel="stylesheet" href="/assets/vendor/redactor2/redactor.css?v={{ hashver }}"/>
    <link href="/assets/vendor/venobox/venobox.css" rel="stylesheet">
    <style>
        body.vbox-open {
            overflow: visible;
        }
        img {
            display: block;
            max-width: 100%;
            height: auto;
        }
    </style>
{% endblock %}

{% block content %}
        <div class="container">
            <form id="fdata" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
                <input type="hidden" id="fcover" name="fcover" value="{{ gallery.gallery_thumbnail }}">
                <input type="hidden" name="gallery_id" value="{{ gallery.gallery_id }}">

                    <div class="row">
	                    
                        <div class="col-sm-8">
                            <div class="form-group ">
                                <label>Наименование<span style="color: red;">*</span></label>
                                <div class="col-sm-9">

                                    <input class="form-control" name="gallery_title" required="" data-errorbox="#errname"
                                           data-msg-required="Наименование обязательно" placeholder="" type="text" value="{{ gallery.gallery_title }}">

                                    <div id="errname" style="color:red;"></div>
                                </div>

                            </div>

                            <div class="form-group ">

                                <div class="col-xs-12">
                                    <textarea name="gallery_desc" id="notes" class="form-control" rows="6">{{ gallery.gallery_desc }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4" style="text-align: center;">
                            <legend class="text-blue text-left">Обложка</legend>
                         
                            <div id="cover-pic" class="bottom fw-grid-gallery" >
                                <img class="img-responsible" id="u-ava" {%if gallery.gallery_thumbnail=="nopic.jpg"%}src="/img/nopic.jpg"{%else%}src="/userfiles/1/pic/{{gallery.gallery_thumbnail}}"{%endif%}>

                            </div>
                       
                            <a style="margin-top:30px;" href="#" class="btn btn-block btn-primary"  data-toggle="modal" data-target="#md-custom">Редактировать обложку</a>
                            
                          
                        </div>
                    </div>
                <div class="col-md-12" style="margin-bottom:5px;">
                    <div class="panel-heading">
                        <span class="title">Фото альбома</span>
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-12 gallery-container" id="d-content" style="padding-left: 0px; padding-right: 0px">
                            {% for p in gallery_pic %}

                                <input type="hidden" name="fpic[]" id="inp_{{p.gallery_pic_id}}" value="{{p.gallery_pic_pic}}">
                                <div class="item" id="item{{p.gallery_pic_id}}" data-id="{{p.gallery_pic_id}}">
                                    <div class="photo">
                                        <div class="img">
                                            <img id="pic" src="{{ihost}}/userfiles/1/pic/{{p.gallery_pic_pic}}" value="{{p.gallery_pic_pic }}">
                                            <div class="over" style="background:  rgba(25, 52, 98, 0.8);">
                                                <div class="func">
                                                    <a class="image-zoom"><i class="icon s7-trash" style="font-size:40px; color: #ed1515b3; line-height:1.2" data-id="{{p.gallerypic_id}}"></i></a>
                                                    <a href="{{ihost}}/userfiles/1/pic/{{p.gallerypic_pic}}" class="image-zoom2"><i class="icon s7-expand1" style="font-size:40px; line-height:1.2"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            {% endfor %}
                        </div>

                        <div class="col-sm-12 text-center">

                            <div class="progress  progress-striped active" id="progress2" style="display:none;">
                                <div id="progressSlide2" style="width: 0%;" class="progress-bar progress-bar-success">Загрузить</div>
                            </div>

                            <div id="counteinerpic2">

                                <button style="clear:both;" class="btn btn-success btn-lg"
                                        id="pickfiles2">Загрузить картинки</button>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="padding-left: 0px;padding-right: 0px;">
                    <button type="button" id="doSave"
                            class="btn btn-primary btn-lg pull-right">Сохранить</button>
                    <a href="/gallery" class="btn btn-default btn-lg pull-left">Отмена</a>
                    <div style="clear:both;margin-bottom: 20px;"></div>
                </div>
            </form>
        </div>




    {% endblock %}



    {% block js %}

        {#<script src="/assets/lib/bootstrap-growl/bootstrap-growl.min.js"></script>#}
        <script src="/assets/vendor/plupload-2.3.1/js/plupload.full.min.js"></script>
        <script src="http://jcrop-cdn.tapmodo.com/v2.0.0-RC1/js/Jcrop.js"></script>
        <script type="text/javascript" src="/assets/vendor/redactor2/redactor.js?v={{ hashver }}"></script>
        <script src="/assets/vendor/masonry/masonry.pkgd.min.js" type="text/javascript"></script>
        <script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>

		{#<script src="/assets/lib/venobox/venobox.min.js"></script>#}
        <script src="/js/gallery_edit.js?v={{ hashver }}"></script>


    {% endblock %}
    {%block modals%}

        <form id="fcoord">
            <input type="hidden" name="fname" id="fname">
            <input type="hidden" name="ftype" id="ftype">
            <input type="hidden" name="fheight" id="fheight">
            <input type="hidden" name="fwidth" id="fwidth">
        </form>

        <div id="md-custom" tabindex="-1" role="dialog" class="modal fade" >
            <div style="width: 100%;" class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body" >
                        <div class="text-center" >
                            <img class="img-responsible center-block" id="target" {%if gallery.gallery_thumbnail=="nopic.png"%}src="/img/avatar/avatar2x200.jpg"
                                    {% else %}
                                        src="/userfiles/1/pic/{{gallery.gallery_thumbnail}}"
                                    {%endif%}>

                        </div>
                    </div>
                    <div class="modal-footer" id="counteinerpic" >
                        <div class="progress  progress-striped active" id="progress1" style="display:none;">
                            <div id="progressSlide" style="width: 0%;" class="progress-bar progress-bar-success">Loading...</div>
                        </div>

                        <div id="btns">

                            <button type="button" id="btnCancel" class="btn btn-default pull-left"><i class="material-icons">close</i></button>

                            <button type="button" class="btn btn-danger pull-left" id="del-pic" style="display: none;"><i class="material-icons">delete_forever</i></button>


                            <button type="button" id="pickfiles"  class="btn btn-primary"><i class="material-icons">file_upload</i></button>

                            <button type="button" id="picSave"  class="btn btn-success" style="display: none;"><i class="material-icons">save</i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {%endblock%}

