{% for r in records %}

<div class="item div{{ r.gallery_id }}" data-id="{{ r.gallery_id }}">
    <div class="photo">
        <div class="img">
            <img {%if r.gallery_thumbnail=="nopic.jpg"%}src="/img/nopic.jpg"
                    {% else %}
                        src="/userfiles/1/pic/{{r.gallery_thumbnail}}"
                    {%endif%}>
            {% if acs.user_id %}
            <div class="over" style="background: rgba(25, 52, 98, 0.8);">
                <div class="func">
                    <a href="#" class="image-zoom trash"  data-id="{{ r.gallery_id }}"><img src="./img/delete.png" data-id="{{ r.gallery_id }}"/></a>
                    <a href="#" class="image-zoom edit" data-id="{{ r.gallery_id }}"><img src="./img/edit.png" data-id="{{ r.gallery_id }}"/></a>
                    <a href="#" data-id="{{ r.gallery_id }}"><img src="./img/eye.png" data-id="{{ r.gallery_id }}"/></a>
                </div>
            </div>
            {% else %}
            <div class="over" style="background: rgba(25, 52, 98, 0.8);">
                <div class="func">
                    <a href="#" data-id="{{ r.gallery_id }}"><img src="/img/nopic.jpg" data-id="{{ r.gallery_id }}"/></a>
                </div>
            </div>
            {% endif %}
        </div>
        <div class="description">

            {#<div class="am-checkbox" style="padding-left: 7px;">#}
                {#<input type="checkbox" id="chk{{r.gallery_id}}" class="check" name="row[]" value="{{r.gallery_id}}">#}
                {#<label for="chk{{r.gallery_id}}"></label>#}
            {#</div>#}

            <div class="desc">
                <p>{{ r.gallery_title }}</p>

                {% if r.gallery_desc %}
                <p>{{ r.gallery_desc }}</p>
                {% endif %}

            </div>
        </div>
    </div>
</div>
{% endfor %}
<input type="hidden" name="gallery_id" data-id="{{ gallery.gallery_id}}" value="{{ gallery.gallery_id }}">
<input type="hidden" id="_idt" value="{{ lng_gallery.del_gallery_title }}">
<input type="hidden" id="_idm" value="{{ lng_gallery.del_gallery_message }}">
<input type="hidden" id="_yes" value="{{ lng_common.btn_yes }}">
<input type="hidden" id="_cb" value="{{ lng_common.btn_cancel }}">