{% extends skin~"/common/root.tpl" %}
{#{% block headcss %}#}
    {#<style>#}
        {#body.vbox-open {#}
            {#overflow: visible;#}
        {#}#}
        {#img {#}
            {#display: block;#}
            {#max-width: 100%;#}
            {#height: auto;#}
        {#}#}
    {#</style>#}
{#{% endblock %}#}
{% block content %}
    {% if acs.user_id %}
    <div class="container page-head">
            <a href="/gallery/add" class="btn btn-success pull-right">Add Album</a>
    </div>
    {% endif %}
    <div class="container">
        <div class="row">
            <input type="hidden" id="_gallerydeltittle" value="Удаление">
            <input type="hidden" id="_gallerydelmes" value="Вы уверены что хотите удалить выбраные альбомы">
            <input type="hidden" id="_yes" value="Да">
            <input type="hidden" id="_no" value="No">

            <form id="fsearch" class="form-horizontal group-border-dashed col-12">

                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon btn-primary"
                              style="background-color:#ccc; color:#fff; font-size: 20px;"><span
                                    class="s7-search"></span></span>
                            <input class="form-control" id="s" placeholder="Поиск"
                                   {% if searchstr %}value="{{ searchstr }}"{% endif %} type="text"
                                   style="border-width:1px;">
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="container">
            <form id="flist">
                <div class="gallery-container row" id="d-content"></div>
                <div style="text-align:center">
                    <button id="load-more" data-next="" style="display:none;"
                            class="btn btn-lg btn-primary">Загрузить больше
                    </button>

                </div>
            </form>
    </div>
    <div style="clear:both;"></div>
{% endblock %}


{% block footer %}
    <div id="megafooter">

    </div>
{% endblock %}


{% block js %}
    <script src="/assets/vendor/masonry/masonry.pkgd.min.js" type="text/javascript"></script>
    <script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
    <script src="/js/gallery.js?v={{ hashver }}"></script>



{% endblock %}