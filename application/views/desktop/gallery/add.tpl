{% extends skin~"/common/root.tpl" %}

{% block css %}
    <link rel="stylesheet" type="text/css" href="/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/vendor/sweetalert/sweetalert.min.css">
    <link rel="stylesheet" href="/assets/vendor/redactor2/redactor.css?v={{ hashver }}"/>
    <link href="/assets/vendor/venobox/venobox.css" rel="stylesheet">
    <style>
        body.vbox-open {
            overflow: visible;
        }
        img {
            display: block;
            max-width: 100%;
            height: auto;
        }
    </style>

{% endblock %}

{% block content %}
    <div class="container">
        <form id="fdata" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
            <input type="hidden" id="fcover" name="fcover" value="nopic.jpg">

            <div class="row">
                <div class="col-md-8">
                    <label>Наименование<span style="color: red;">*</span></label>

                    <div>
                        <input class="form-control" name="gallery_title" required=""
                               data-errorbox="#errname"
                               data-msg-required="Наименование обязательно"
                               placeholder="" type="text">

                        <div id="errname" style="color:red;"></div>
                    </div>
                            <textarea name="gallery_desc" id="notes" class="form-control"
                                      rows="6"></textarea>
                </div>

                <div class="col-md-4" style="text-align: center;">
                    <legend class="text-blue text-left">Обложка</legend>
                    <div id="cover-pic">
                        <img class="img-responsible" id="u-ava" src="/img/nopic.jpg">
                    </div>
                    <div style="clear:both;margin-top: 20px;"></div>
                    <a href="#" class="btn btn-block btn-primary" data-toggle="modal"
                       data-target="#md-custom">Загрузить Обложку</a>

                </div>
            </div>
            <div class="col-md-12">

                <span class="title">Фото альбома</span>

                <div class="gallery-container" id="d-content"></div>


                <div class="col-sm-12 text-center">

                    <div class="progress  progress-striped active" id="progress2" style="display:none;">
                        <div id="progressSlide2" style="width: 0%;" class="progress-bar progress-bar-success">
                            Загрузка
                        </div>
                    </div>

                    <div id="counteinerpic2">

                        <button style="clear:both;" class="btn btn-success btn-lg"
                                id="pickfiles2">Загрузить фото
                        </button>
                    </div>

                </div>
            </div>
            <div class="col-md-12">
                <button type="button" id="doSave"
                        class="btn btn-primary btn-lg pull-right">Сохранить
                </button>
                <a href="/gallery" class="btn btn-default btn-lg pull-left">Отменить</a>

                <div style="clear:both;margin-bottom: 20px;"></div>
            </div>
        </form>
    </div>


{% endblock %}



{% block js %}
    <script src="/assets/vendor/plupload-2.3.1/js/plupload.full.min.js"></script>
    {#<script src="http://jcrop-cdn.tapmodo.com/v2.0.0-RC1/js/Jcrop.js"></script>#}
    <script type="text/javascript" src="/assets/vendor/redactor2/redactor.js?v={{ hashver }}"></script>
    {#<script src="/assets/lib/switch/js/bootstrap-switch.min.js"></script>#}
    <script src="/assets/vendor/masonry/masonry.pkgd.min.js" type="text/javascript"></script>
    <script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
    <script src="/assets/vendor/venobox/venobox.min.js"></script>
    <script src="/js/gallery_add.js"></script>



{% endblock %}
{% block modals %}

    <form id="fcoord">
        <input type="hidden" name="fname" id="fname">
        <input type="hidden" name="ftype" id="ftype">
        <input type="hidden" name="fheight" id="fheight">
        <input type="hidden" name="fwidth" id="fwidth">
    </form>

    <div id="md-custom" tabindex="-1" role="dialog" class="modal fade">
        <div style="width: 100%;" class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="text-center">
                        <img class="img-responsible" id="target"
                             {% if gallery.gallery_thumbnail=="nopic.jpg" %}src="/img/nopic.jpg"{% endif %}>

                    </div>
                </div>
                <div class="modal-footer" id="counteinerpic">
                    <div class="progress active" id="progress1" style="display:none;">
                        <div id="progressSlide" style="width: 0%;" class="progress-bar progress-bar-success">
                            Loading...
                        </div>
                    </div>

                    <div id="btns">

                        <button type="button" id="btnCancel" class="btn btn-default pull-left">Закрыть</button>

                        <button type="button" class="btn btn-danger pull-left" id="del-pic" style="display: none;">
                            Удалить
                        </button>


                        <button type="button" id="pickfiles" class="btn btn-primary">Загрузить
                        </button>

                        <button type="button" id="picSave" class="btn btn-success" style="display: none;">Сохранить
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

{% endblock %}
