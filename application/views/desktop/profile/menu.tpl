<div class="col-md-2 navigation">
    <h3 class="heading center">Налаштування<span></span></h3>
    <ul class="nav-tabs vertical" role="tablist">
        <li class="{{smProfile}}"><a href="/profile">Аккаунт<span class="line"></span></a></li>
        <li class="{{smPosts}}"><a href="/posts">Публікації<span class="line"></span></a></li>
        {%if acs.user_role!="user"%}
        <li class="{{smPosters}}"><a href="/poster">Афіша<span class="line"></span></a></li>{%endif%}
        <li class="{{smFavorites}}"><a href="/favorites">Обране<span class="line"></span></a></li>
    </ul>
</div>
