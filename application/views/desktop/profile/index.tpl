{% extends skin~"/common/root.tpl" %}
{%block css%}
<link href="/js/lib/crope/cropper.css" rel="stylesheet">
{%endblock%}


{% block content %}
<section class="page-block user-account double-space-bottom">
    <div class="container">
        <div class="row">

            {%include skin~"/profile/menu.tpl"%}


            <!--Content-->
            <div class="col-md-9 gray-bg">
                <div class="tab-content">

                    <!--Account Settings-->
                    <div class="tab-pane fade in active" id="account">
                        <h3 class="heading center">Налаштування аккаунта<span></span></h3>
                        <form class="account-settings" id="fprofile" enctype="multipart/form-data">
                            <input type="hidden" id="user_id" name="user_id" value="{{acs.user_id}}">
                            <div class="row">
                                <div class="col-md-7 col-sm-7 space-bottom">
                                    <fieldset>
                                        <div class="form-group">
                                            <label for="af-first-name">Ім'я </label>
                                            <input class="form-control" type="text" value="{{acs.user_fname}}"
                                                   name="user_fname" id="user_fname">
                                        </div>
                                        <div class="form-group">
                                            <label for="af-last-name">Прізвище </label>
                                            <input class="form-control" type="text" value="{{acs.user_lname}}"
                                                   name="user_lname" id="af-last-name">
                                        </div>
                                        <div class="form-group">
                                            <label for="af-screen-name">Нік <span class="red">*</span></label>
                                            <input class="form-control" type="text" name="user_nick"
                                                   value="{{acs.user_nick}}" id="user_nick" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="af-email">Email <span class="red">*</span></label>
                                            <input class="form-control" type="email" name="user_email"
                                                   value="{{acs.user_email}}" id="user_email" required>
                                        </div>
                                    </fieldset>

                                </div>
                                <div class="col-md-5 col-sm-5">
                                    <input type="hidden" name="img" id="img-file">
                                    <div class="user-avatar" id="containerpic">
   <span {%if acs.user_pic == "nopic.jpg"%}style="display:none;"{%endif%} id="delAva" class="delete"></span>

    <div id="avatar-box">
    {%if acs.user_pic == "nopic.jpg"%}
    <img class="avatar" id="preview" src="/img/nopic.jpg" alt="Аватар"/>
    {%else%}
    <img class="img-responsive center-block" src="/userfiles/{{acs.user_id}}/avatar/{{acs.user_pic}}" alt="Користувач"/>
    {%endif%}
</div>
    <a class="upload" id="pickfiles"><i class="fa fa-upload"></i><span>Завантажити фото</span></a>
</div>

<!--<div class="form-group">
<label >Додати Фото</label>
<input type="hidden" name="img" id="img-file">
<div class="form-group">
<div id="containerpic">
<img class="img_responsive" id="preview" src="/img/nopic.jpg" style="display:none;">
<button id="pickfiles" class="btn btn-primary">Upload Image</button>
</div>
</div>
</div>-->
</div>
</div>
<div class="row space-top">
<div class="col-md-7 col-sm-7">
<h3 class="heading center">Атрибути<span></span></h3>
<div class="form-group">
<div class="row">
<div class="col-md-12"><label>Дата народження    </label></div>
</div>
<div class="row">
<div class="col-md-3 form-group">
<div class="select-style">
<select name="db_day" id="db_day">
{%for i in 1..31%}
<option {%if acs.user_birth|date("j")==i%}selected{%endif%} value="{{i}}">{{i}}</option>
{%endfor%}
</select>
</div>
</div>
<div class="col-md-5 form-group">
<div class="select-style">
<select name="db_month" id="db_month">
{%for m in months%}
<option {%if acs.user_birth|date("n")==m.m%}selected{%endif%} value="{{m.m}}">{{m.name}}</option>

{%endfor%}

</select>
</div>
</div>
<div class="col-md-4 form-group">
<div class="select-style">
<select name="db_year" id="db_year">
{%for i in 1920..2010%}
<option {%if acs.user_birth|date("Y")==i%}selected{%endif%} value="{{i}}">{{i}}</option>
{%endfor%}
</select>
</div>
</div>
</div>
<label>Стать</label>
<div class="form-group">
<div class="radio">
<label><input type="radio" name="user_gender" id="user_gender" {%if acs.user_gender=="m"%}checked{%endif%} value="m" > Чоловік</label>
</div>
<div class="radio">
<label><input type="radio" name="user_gender" id="user_gender" {%if acs.user_gender=="f"%}checked{%endif%} value="f"> Жінка</label>
</div>
</div>

</div>
</div>
<div class="col-md-5 col-sm-5">
<h3 class="heading center">Місцезнаходження<span></span></h3>
<div class="form-group">
<label for="af-country">Країна </label>
<div class="select-style">
<select name="country" id="country">
{%for c in clist%}
<option {%if c.country_iso==acs.user_country%}selected{%endif%} value="{{c.country_iso}}">{{c.country_printable_name}}</option>
{%endfor%}
</select>
</div>
</div>
<div class="form-group">
<label for="af-state">Земля / Провінція</label>
<input class="form-control" type="text" value="{{acs.user_state}}" name="user_state" >
						</div>
                          <div class="form-group">
                            <label for="af-city">Місто</label>
                            <input class="form-control" type="text" value="{{acs.user_city}}"name="user_city" >
                          </div>
                          <div class="form-group">
                            <label for="af-city">Адреса</label>
                            <input class="form-control" type="text" value="{{acs.user_street}}" name="user_street" >
                          </div>

                          <div class="form-group">
                            <label for="af-zip">Почтовий Індекс</label>
                            <input class="form-control" type="text" value="{{acs.user_zip}}" name="user_zip" >
                          </div>
                        </div>
                       </div>
                       <div class="row space-top">
                       	<div class="col-md-3 col-sm-3 space-bottom">
                        	
                         
                        </div>
                        <div class="col-md-6 col-sm-6">
                        	<div class="text-center">
                        
                              
                              <button class="btn btn-md btn-default2" type="reset" >Скасувати</button>
                              <button class="btn btn-md btn-primary" id="doSave" type="submit" >Зберегти</button>
                          </div>
                        </div>
                       </div>
                      </form>
                    </div>
                    

                    
                    
                  </div>
                </div>
              </div>
          	</div>
          </section>
{%endblock%}

{%block js%}


<script src="/js/lib/plupload-2.1.9/plupload.full.min.js"></script>
<script src="/js/lib/bootbox.min.js"></script>
<script src="/js/lib/crope/cropper.js"></script>

<script src="/js/profile.js"></script>
{%endblock%}
