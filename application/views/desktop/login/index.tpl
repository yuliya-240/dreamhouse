{% extends skin~"/common/root.tpl" %}
{%block css%}
<style>
    .error {
        color: red;
    }
</style>
{%endblock%}
{%block content%}
<section class="specialty-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                <div class="page-heading">
                    <h2>Логін</h2>
                    <h3>Будь-ласка увійдіть для того щоб отримати доступ до додаткових можливостей</h3>
                </div>


                <form class="login-form space-bottom" id="flogin">
                    <div class="form-group">
                        <label class="sr-only" for="email"></label>
                        <input type="email" class="form-control input-lg" name="email" id="email"
                               placeholder="E-mail" required>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="password"></label>
                        <input type="password" class="form-control input-lg" name="password" id="password" placeholder="Пароль"
                               required>
                    </div>
                    <input class="btn btn-md btn-primary btn-center" type="submit" value="Увійти">
                </form>
            </div>
        </div>
    </div>
</section>
{%endblock%}

{%block js%}
<script src="/js/login.js"></script>
{%endblock%}
