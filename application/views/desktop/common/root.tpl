<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>DreamHouse - Комплексный ремонт квартир и коттеджей</title>

    <link rel="apple-touch-icon" sizes="57x57" href="/img/icon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/img/icon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/icon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/icon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/icon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/icon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/icon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/icon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/icon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/img/icon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/img/icon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/icon/favicon-16x16.png">
    <link rel="manifest" href="/img/icon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/img/icon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" type="text/css" href="/assets/vendor/select2/css/select2.css"/>
    <link rel="stylesheet" type="text/css" href="assets/vendor/bootstrap/css/bootstrap.css">
    <link type="text/css" href="assets/vendor/sweetalert/sweetalert.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    {#<link href="/css/jumbotron.css" rel="stylesheet">#}
    {% block headcss %}{% endblock %}
    <link rel="stylesheet" type="text/css" href="/css/style.css">


</head>

<body style='overflow-x:hidden;'>
{%block css%}{%endblock%}
{#{% include skin~"/common/headtoolbar.tpl" %}#}
{% include skin~"/common/nav.tpl" %}
<main role="main">

{%block content%}



{%endblock%}
</main>


<!-- Footer -->
{% include skin~"/common/footer.tpl" %}

{% include skin~"/common/contact.tpl" %}


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="/assets/vendor/jquery/jquery-3.2.1.min.js"></script>
<script src="/assets/vendor/jqueryui/jquery-ui.min.js"></script>
{#<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>#}
{#<script>window.jQuery || document.write('<script src="/assets/js/jquery-slim.min.js"><\/script>')</script>#}
<script src="/assets/js/popper.min.js"></script>
<script src="/assets/vendor/select2/js/select2.js" type="text/javascript"></script>

<script src="/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="/assets/vendor/jquery-validate/jquery.validate.min.js"></script>
<script src="/assets/vendor/sweetalert/sweetalert.min.js"></script>
<script src="/assets/vendor/plupload-2.3.1/js/plupload.full.min.js"></script>
{#<script type="text/javascript" src="/assets/vendor/select2/select2.min.js"></script>#}
{#<script src="/assets/vendor/select2/js/select2.js" type="text/javascript"></script>#}
{%block js%}{%endblock%}
<script type="text/javascript">
    $('#qcf-btn').click(function(){
        $(this).parent().find('.quick-contact').toggleClass('visible');
    });

</script>
<script type="text/javascript">
    if(window.matchMedia('(min-width: 992px)').matches) {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 500) {

                $('#scrollTop-btn').parent().addClass('scrolled');
            } else {
                $('#scrollTop-btn').parent().removeClass('scrolled');
            }
            if ($(this).scrollTop() > 112) {

                $('.navbar').addClass('fixed-top');
                $('.headtoolbar').hide();
            } else {
                $('.headtoolbar').show();
                $('.navbar').removeClass('fixed-top');
            }
        });
        $('#scrollTop-btn').click(function () {

            $('html, body').animate({scrollTop: 0}, 300);
        });
    }
</script>
<script type="text/javascript">
    document.ondragstart = noselect;
    document.onselectstart = noselect;
    document.oncontextmenu = noselect;
    function noselect() {return false;}
</script>

{%block modals%}{%endblock%}
</body>
</html>