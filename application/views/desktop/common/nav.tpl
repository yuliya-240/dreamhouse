<nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
    <div class="col-12">
    <div class="row headtoolbar" style="text-align: center">
        {#<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample10" aria-controls="navbarsExample10" aria-expanded="false" aria-label="Toggle navigation">#}
            {#&#123;&#35;<span class="navbar-toggler-icon"></span>&#35;&#125;Открыть контакты#}
        {#</button>#}
        {#<div class="collapse navbar-collapse justify-content-md-center" id="navbarsExample10">#}
        <div class="col-md-4" style="display:contents">

            <img src="/img/icons/logo2.png" alt="IMG-LOGO">

        </div>
        <div class="col-md-2">
            <a title="Viber"  href="viber://chat?number=+380950556886"><img src="/img/002-viber.png">
            </a>

            <a title="WhatsApp" href="whatsapp://send?phone=+380990805998"><img src="/img/001-whatsapp.png"></a>
            <a title="Facebook" href="https://www.facebook.com/groups/190467651728576/"><img src="/img/003-facebook.png"></a>


        </div>

				<span class="col-md-3">
                    +38 (095) 055-68-86<br>
                    +38 (099) 080-59-98
				</span>

        <div class="col-md-3">
            <p>Напишите нам</p>
					<span class="topbar-email">
						<a href="mailto:dreamhousesumy@gmail.com">dreamhousesumy@gmail.com</a>
					</span>

        </div>
    {#</div>#}
    </div>
    <div class="row">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample10" aria-controls="navbarsExample10" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse justify-content-md-center" id="navbarsExample10">
        <ul class="navbar-nav">
            <li class="nav-item {{topHome}}">
                <a class="nav-link" href="/">Главная</a>
            </li>
            <li class="nav-item {{topPrice}}">
                <a class="nav-link" href="/price">Цены и Услуги</a>
            </li>
            <li class="nav-item {{topGallery}}">
                <a class="nav-link" href="/gallery">Фотогалерея</a>
            </li>
            {#<li class="nav-item {{ topComment }}">#}
                {#<a class="nav-link" href="/comments">Отзывы</a>#}
            {#</li>#}

            {%if acs.user_id%}
                <li class="nav-item"><a class="nav-link" href="/logout" style="color: red;">Выход</a></li>
            {%endif%}

        </ul>
    </div>
    </div>
    </div>
</nav>
