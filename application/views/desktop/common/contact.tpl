<div class="sticky-btns">
    <form class="quick-contact" method="post" name="quick-contact" style="z-index: auto;">
        <h3>Обратная Связь</h3>
        <div class="form-group">
            <label for="qc-name">Имя</label>
            <input class="form-control" type="text" name="qc-name" id="qc-name" placeholder="Введіть свое ім'я"
                   required>
        </div>
        <div class="form-group">
            <label for="qc-email">Email</label>
            <input class="form-control" type="email" name="qc-email" id="qc-email" placeholder="Введіть email" required>
        </div>
        <div class="form-group">
            <label for="qc-tel">Телефон</label>
            <input class="form-control" type="text" name="qc-tel" id="qc-tel" placeholder="Введіть телефон" required>
        </div>
        <div class="form-group">
            <label for="qc-message">Сообщение</label>
            <textarea class="form-control" name="qc-message" id="qc-message" placeholder="Напишіть Ваше повідомлення"
                      required></textarea>
        </div>
        <button class="btn btn-danger btn-block" id="doSendContact" type="submit" >Отправить</button>
    </form>
    <span id="qcf-btn"><img src="/img/004-send.png"></span>
    <span id="scrollTop-btn"><img src="/img/003-up-arrow.png"></span>
</div><!--Sticky Buttons Close-->
