<footer class="blog-footer" style="text-align: center" xmlns="http://www.w3.org/1999/html"
        xmlns="http://www.w3.org/1999/html">
    <div class="row">
        <div class="col-md-4">
            <a title="Viber"  href="viber://chat?number=+380950556886"><img src="/img/002-viber.png">
            </a>

            <a title="WhatsApp" href="whatsapp://send?phone=+380990805998"><img src="/img/001-whatsapp.png"></a>
            <a title="Facebook" href="https://www.facebook.com/groups/190467651728576/"><img src="/img/003-facebook.png"></a>

        </div>

        <span class="col-md-4">

			<a href="tel:+380950556886">+38 (095) 055-68-86<br></a>
            <a href="tel:+380990805998">+38 (099) 080-59-98</a>
        </span>

        <div class="col-md-4">
            <span class="topbar-email">

				<a href="mailto:dreamhousesumy@gmail.com">dreamhousesumy@gmail.com</a>
			</span>
        </div>
    </div>
</footer>

