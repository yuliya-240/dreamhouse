<?php
namespace jet\rpc\json;

use \jet\json as json;

class client {
    
    private $url = null;
    private $session_id = null;
    private $debug_log = '';
    private $debug = false;
    
    public function __construct($url = 'http://localhost/', $session_id = null, $debug = false) {
        $this->url = $url;
        $this->session_id = $session_id;
        $this->debug = $debug;
    }   
    
    private function trace($value) {
        $this->debug_log .= $value."\n";
    }
    
    public function getTrace() {
        return $this->debug_log;    
    }              
    static public function prepareRequest($method, $params, $id) {   
        if (!is_scalar($method)) {
            throw new \Exception('Wrong method mame');
        }
        
        if (is_array($params)) {
            $params = array_values($params);
        } else {
            throw new \Exception('Params must be an array');
        }
        
        $request = array(   
            'method' => $method,
            'params' => $params,
            'id'     => $id
        );    
        
        return json::encode($request);        
    }
    public function __call($method, $params) {    
        
        $request = self::prepareRequest($method, $params, $this->session_id );     
        
        $this->trace("[REQUEST]".$request."[/REQUEST]");
        
        $opts = array ('http' => array (
            'method'  => 'POST',
            'header'  => 'Content-type: application/json',
            'content' => $request
        ));
                        
        $context  = stream_context_create($opts);                                                            

        if ($fp = @fopen($this->url, 'r', false, $context)) {
            
            $response = '';
            
            while($row = fgets($fp)) {
                $response.= trim($row)."\n";
            }
            
            $this->trace("[SERVER RESPONSE]".$response."[/SERVER RESPONSE]");      
            
            $response = json::decode($response);
            
            if (!$response) {
                throw new \Exception('RPC.Response.InvalidJSON');    
            }
            
        } 
        else {                         
            throw new \jet\exception('Unable to connect to server', E_USER_WARNING, array('url'=>$this->url));   
        }

        
        if ($this->session_id !== null) {
            if (isset($response) && is_array($response)) {
                
                if (!isset($response['id'])) {
                    throw new \Exception('Wrong response structure. Fields id or result are missing.');   
                    return null;                        
                }
                
                if ($response['id'] != $this->session_id) {
                    throw new \Exception('Wrong Session Id. Current: ['.$this->session_id.'] Recieved: ['.$response['id'].']');
                    return null;
                }
                
                if (!is_null($response['error'])) {
                    throw new fault($response['error']);
                    return null;
                }   
                
                return $response['result'];    
            }
            else {
                throw new \Exception('Response was not in JSON');    
                return null;                
            }
        }
        else {
            return true;    
        } 
    }  
}
