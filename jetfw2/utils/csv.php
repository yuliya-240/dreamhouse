<?php
namespace jet\utils; 

class csv {  
    public static function detectDelimiterFile($file) {
        $lines = array(); 
        if (($handle = fopen($file, "r")) !== FALSE) {    
            for ($i = 0; $i<3; $i++) {
                $lines[] = fgets($handle);
            }
            fclose($handle);  
        } 
        else return false;
        
        $delimiters = array(";"=>0,","=>0,"\t"=>0);
        
        foreach ($delimiters as $delimiter => $val) {
            $count = -1;         
            $match = 0;
            foreach($lines as $line) {                    
                $parsed = str_getcsv($line,$delimiter);                
                if ($count == -1) $count = count($parsed);
                if (count($parsed) == $count) $match++;
            }                                          
            if ($match == 3) $delimiters[$delimiter] = $count;
        }                   
        
        $max = ';';
        foreach ($delimiters as $delimiter => $val) { 
            if ($val > $delimiters[$max]) $max = $delimiter;
        }          
        return $max;  
    }
    
    
    public static function fgetcsv($f, $length, $d=",", $q='"') {
        $list = array();
        $st = fgets($f, $length);
        $st = self::toUtf8($st);
        $st = preg_replace('/[\\r\\n]*?$/',"",$st);
        
        if ($st === false || $st === null) return $st;
        if (!$st) return false;

        if ((float)PHP_VERSION >= 5.3) {
          $list = str_getcsv($st, $d, $q);
        } else {      
        
          //$st = charset_x_win($st);    
          //$st = iconv('cp1251', 'UTF-8', $st);

          
          while ($st !== "" && $st !== false) {
              if ($st[0] !== $q) {
                  # Non-quoted.
                  list ($field) = explode($d, $st, 2);
                  $st = substr($st, strlen($field)+strlen($d));
              } else {
                  # Quoted field.
                  $st = substr($st, 1);
                  $field = "";
                  while (1) {
                      # Find until finishing quote (EXCLUDING) or eol (including)
                      preg_match("/^((?:[^$q$d]+|$q$q)*)/sx", $st, $p);
                      $part = $p[1];
                      if (substr($part, strlen($part)-1, 1) == $q) {
                        $part = substr($part, 0, strlen($part)-1);
                        $p[0] = substr($p[0], 0, strlen($part)-1);
                      }
                      $partlen = strlen($part);
                      $st = substr($st, strlen($p[0]));
                      $field .= str_replace($q.$q, $q, $part);
                      if (strlen($st) && $st[0] === $q) {
                          # Found finishing quote.
                          list ($dummy) = explode($d, $st, 2);
                          $st = substr($st, strlen($dummy)+strlen($d));
                          break;
                      } else {
                          # No finishing quote - newline.
                          $st = fgets($f, $length);
                      }
                  }
  
              }
              $list[] = $field;
          }
        }
        return $list;
    }

    public static function fputcsv($f, $list, $d=",", $q='"') {
        $line = ""; 
        foreach ($list as $field) { 
            # remove any windows new lines, 
            # as they interfere with the parsing at the other end 
            $field = str_replace("\r\n", "\n", $field); 
            # if a deliminator char, a double quote char or a newline 
            # are in the field, add quotes 
            if(ereg("[$d$q\n\r]", $field)) { 
                $field = $q.str_replace($q, $q.$q, $field).$q; 
            }
            $line .= $field.$d; 
        }
        # strip the last deliminator 
        $line = substr($line, 0, -1); 
        # add the newline 
        $line .= "\n"; 
        # we don't care if the file pointer is invalid, 
        # let fputs take care of it 
        return fputs($f, $line); 
    }
    
    public static function toUtf8($str) {
        if (mb_check_encoding($str, 'UTF-8')) return $str;
        else { 
            $st2 = iconv('cp1251','utf-8',$str); 
            if (mb_check_encoding($st2, 'UTF-8')) return $st2;   
        }   
        return $str;  
    }
}

?>
