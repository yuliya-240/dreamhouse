<?php
namespace jet\view\jet;

class twig {
    private $twig;
    public function __construct($twig) {
        $this->twig = $twig;
    }
    public function __toString() {
        try {
            return  $this->twig->render();
        }
        catch (\exception $e) {
            return (string)$e;        
        }        
    }
}