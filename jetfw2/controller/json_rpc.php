<?php    
namespace jet\controller; 
use \jet\rpc\json\server as JSONRPCServer;  
use \jet\json as json;

/* Класс JSON RPC контроллера */
class json_rpc extends \jet\controller {    
    
    public function __construct() {
        parent::__construct();
    }     
  
    public function __setup($name, $method = '__default', $args = array()) {
        
        if (!JSONRPCServer::checkRequest()) {
            
           throw new \exception("Not JSON-RPC request"); 
        }  
                              
        $this->__setSession(JSONRPCServer::getSessionId());

        $this->name = $name;
       
        $this->method = JSONRPCServer::getRequestMethod(); 
                
        $this->args = JSONRPCServer::getRequestParams();        
                        
              
    }
    
    protected function __setSession($session_id) {
        /*
        if (\session_id()) {
          \session_destroy();
        }
                       
        $session_prefix = \str_ireplace('\\','-', \get_class($this)).'-';
        \session_id($session_prefix.$session_id);
        \session_start();    
        return $session_prefix.$session_id;       */ 
        return $session_id;       
    }
    
    public function __callmethod($method = "__default", $args = array()) {  
        $request = JSONRPCServer::getRequest();
        
        if (!$request) return false;   
        
        if (!method_exists($this, JSONRPCServer::getRequestMethod())) {
            JSONRPCServer::setError('There is no such remote method like ['.JSONRPCServer::getRequestMethod().'] in class ['.get_class($this).']'); 
        } 
		else {
			$result = call_user_func_array(array($this, JSONRPCServer::getRequestMethod()), JSONRPCServer::getRequestParams());			
			JSONRPCServer::setResult($result);
		}
        return JSONRPCServer::getResponse();    
    } 
    
    public function __error($e) {
        JSONRPCServer::setError($e->getMessage(), $e);         
    }
    
    public function __after() {
        header('content-type: application/json');
    }    

}
