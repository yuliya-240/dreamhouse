<?php    
namespace jet;

use \jet\application as app;
use jet\libs\Twig as TwigLib;

class twig extends \jet\obj {
    
    private $twig  = null;    
    private $fault = false;
    private $ready = false;    
    private $template = null;    
    private $path = '';    
    private $globals = array();    
    
    public static $extensions = array();

    
    private function __init() {
        TwigLib::load();
        
        $twig_conf = array(
            'debug'       => app::$settings->debug,
            'auto_reload' => true,
            'autoescape'  => false
        );
        
        if (app::$settings->cache_on) {
            $twig_conf['cache'] = app::$settings->cache_path;
            if (!file_exists(app::$settings->cache_path)) {
                mkdir(app::$settings->cache_path);
                chmod(app::$settings->cache_path, 0755);
            }
        }
        
        $loader = new \Twig_Loader_Filesystem(app::$settings->path._.'views'._); 
         
        $this->twig = new \Twig_Environment($loader, $twig_conf);
        

        $this->twig->addExtension(new \jet\libs\Twig\Extension\jetExtension());
             
           
        $this->ready = true;
    }
    
    private function __prepare() {

        if (!$this->ready) $this->__init();
        if (!$this->template || !file_exists(app::$settings->path._.'views'._.$this->path.$this->template)) {
            $this->fault = true;
        }     
        foreach(self::$extensions as $extension) {
            $this->twig->addExtension($extension);
        }
        foreach($this->globals as $key => $value) {
            $this->twig->addGlobal($key, $value);    
        }
    }
    
    public function render() {  
        $this->__prepare();   
        if ($this->fault) return print_r($this->array, true);     
        $template = $this->twig->loadTemplate($this->path.$this->template);
        return $template->render($this->array);
    }
    
    public static function addExtension(){
        self::$extensions[] = $extension;
    }
    
    public function setPath($path) {
        $path = trim($path,_)._;
        if ($path == _) $path = '';
        $this->path = $path; 
    }
    
    public function getPath() {
        return $this->path;
    }
    
    public function setTemplate($template) {
        $this->template = $template;   
    }
    
    public function getTemplate() {
        return $this->template;   
    }
        
    public function setArray($array) {
        $this->array = $array;        
    }
    
    public function addArray($array) {
        $this->array = array_merge($this->array, $array);   
    }
    
    public function setGlobal($name, $value) {
        $this->globals[$name] = $value;
    }
    
    public function getGlobal($name) {
        return $this->globals[$name];
    }
}
