<?php

/* desktop/price/index.tpl */
class __TwigTemplate_8349bb723941794463d594a00216ce99 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'preheadcss' => array($this, 'block_preheadcss'),
            'content' => array($this, 'block_content'),
            'footer' => array($this, 'block_footer'),
            'js' => array($this, 'block_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate(((isset($context["skin"]) ? $context["skin"] : null) . "/common/root.tpl"));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_preheadcss($context, array $blocks = array())
    {
        // line 3
        echo "    <link rel=\"stylesheet\" type=\"text/css\" href=\"/assets/vendor/select2/css/select2.css\"/>
";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "
    <div class=\"container\" style=\"padding: inherit\">
            <div class=\"row\">
                <div class=\"col-sm-8\">
                    <form id=\"fsearch\"  class=\"form-horizontal group-border-dashed\">
                        <div class=\"form-group\">
                                <div class=\"input-group\">
                                    <span class=\"input-group-addon \" style=\"background-color:#ff614e; color:#fff; \" ><span class=\"fa fa-search\" style=\"font-size: 24px;\"></span></span>
                                    <input class=\"form-control\" id=\"s\" placeholder=\"Поиск\" ";
        // line 14
        if ((isset($context["searchstr"]) ? $context["searchstr"] : null)) {
            echo "value=\"";
            echo (isset($context["searchstr"]) ? $context["searchstr"] : null);
            echo "\"";
        }
        echo " type=\"text\" style=\"border-width:1px;\" >
                                </div>
                        </div>
                    </form>

                </div>

                <div class=\"col-sm-4 form-group\">
                    <div class=\"form-group\">
                        <div class=\"input-group\">
                    <select id=\"cat\" name=\"cat\" class=\"select2\">
                        <option value=\"0\">Все категории</option>
                        ";
        // line 26
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["cats"]) ? $context["cats"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 27
            echo "                            <option ";
            if (((isset($context["ccat"]) ? $context["ccat"] : null) == $this->getAttribute((isset($context["c"]) ? $context["c"] : null), "price_cat_id"))) {
                echo "selected";
            }
            echo " value=\"";
            echo $this->getAttribute((isset($context["c"]) ? $context["c"] : null), "price_cat_id");
            echo "\">";
            echo $this->getAttribute((isset($context["c"]) ? $context["c"] : null), "price_cat_name");
            echo "</option>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 29
        echo "                    </select>
                </div>
                </div>
                </div>

            </div>
            ";
        // line 35
        if ($this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_id")) {
            // line 36
            echo "            <div class=\"row\">
                <button id=\"add\">Добавить услугу</button>
            </div>
        ";
        }
        // line 40
        echo "            <div class=\"row\">
                <div class=\"col-sm-12\">

                    <form id=\"flist\" style=\"border-radius: 0px;\" class=\"form-horizontal group-border-dashed\">
                        <div class=\"panel panel-default panel-borders\">

                            <div class=\"panel-body\">

                                <div id=\"d-content\">

                                </div>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    <div id=\"new-cat-box\" tabindex=\"-1\" role=\"dialog\" class=\"modal modal-colored-header fade\" >
        <div style=\"width: 100%;\" class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" data-dismiss=\"modal\" aria-hidden=\"true\" class=\"close\"><i class=\"icon s7-close\"></i></button>
                    <h3 class=\"modal-title\">Редактирование Записи</h3>
                </div>

                <div class=\"modal-body\">
                    <div class=\"row\">
                        <div class=\"col-xs-12\">
                            <form id=\"fdata\" class=\"form-horizontal\" role=\"form\">

                                <div class=\"form-group edit-cat\">

                                </div>
                            </form>

                        </div>
                    </div>
                </div>

                <div class=\"modal-footer\" >

                    <button type=\"submit\" id=\"doSave\"   class=\"btn btn-primary doShare btn-md \">Сохранить</button>
                    <button type=\"submit\" id=\"doEdit\" style=\"display: none\"  class=\"btn btn-primary doShare btn-md \">Изменить</button>

                    <button type=\"button\"  class=\"btn btn-default btn-md  pull-left \" data-dismiss=\"modal\">Отменить</button>

                </div>



            </div>
        </div>
    </div>


";
    }

    // line 98
    public function block_footer($context, array $blocks = array())
    {
        // line 99
        echo "    <div id=\"megafooter\"></div>

";
    }

    // line 104
    public function block_js($context, array $blocks = array())
    {
        // line 105
        echo "    <script src=\"/js/price.js\" type=\"text/javascript\"></script>

";
    }

    public function getTemplateName()
    {
        return "desktop/price/index.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  175 => 105,  172 => 104,  166 => 99,  163 => 98,  102 => 40,  96 => 36,  94 => 35,  86 => 29,  71 => 27,  67 => 26,  48 => 14,  38 => 6,  35 => 5,  30 => 3,  27 => 2,);
    }
}
