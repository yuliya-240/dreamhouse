<?php

/* desktop/login/index.tpl */
class __TwigTemplate_0b776564de0e9878efc80217a7a0f65b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
            'js' => array($this, 'block_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate(((isset($context["skin"]) ? $context["skin"] : null) . "/common/root.tpl"));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_css($context, array $blocks = array())
    {
        // line 3
        echo "<style>
    .error {
        color: red;
    }
</style>
";
    }

    // line 9
    public function block_content($context, array $blocks = array())
    {
        // line 10
        echo "<section class=\"specialty-page\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12\">
                <div class=\"page-heading\">
                    <h2>Логін</h2>
                    <h3>Будь-ласка увійдіть для того щоб отримати доступ до додаткових можливостей</h3>
                </div>


                <form class=\"login-form space-bottom\" id=\"flogin\">
                    <div class=\"form-group\">
                        <label class=\"sr-only\" for=\"email\"></label>
                        <input type=\"email\" class=\"form-control input-lg\" name=\"email\" id=\"email\"
                               placeholder=\"E-mail\" required>
                    </div>
                    <div class=\"form-group\">
                        <label class=\"sr-only\" for=\"password\"></label>
                        <input type=\"password\" class=\"form-control input-lg\" name=\"password\" id=\"password\" placeholder=\"Пароль\"
                               required>
                    </div>
                    <input class=\"btn btn-md btn-primary btn-center\" type=\"submit\" value=\"Увійти\">
                </form>
            </div>
        </div>
    </div>
</section>
";
    }

    // line 39
    public function block_js($context, array $blocks = array())
    {
        // line 40
        echo "<script src=\"/js/login.js\"></script>
";
    }

    public function getTemplateName()
    {
        return "desktop/login/index.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 40,  72 => 39,  41 => 10,  38 => 9,  29 => 3,  26 => 2,);
    }
}
