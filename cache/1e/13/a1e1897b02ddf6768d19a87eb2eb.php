<?php

/* desktop/price/page.tpl */
class __TwigTemplate_1e13a1e1897b02ddf6768d19a87eb2eb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["records"]) ? $context["records"] : null)) {
            // line 2
            echo "    <div class=\"table-responsive\">
        <form id=\"flist\">
            <table class=\"table  table-hover\">
                <thead>
                <tr>
                    <th>Наименование</th>
                    <th>Ед.изм.</th>
                    <th>Цена(грн.)</th>
                    ";
            // line 10
            if ($this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_id")) {
                // line 11
                echo "                        <th>Действие</th>
                    ";
            }
            // line 13
            echo "                </tr>
                </thead>
                <tbody>

                ";
            // line 17
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["records"]) ? $context["records"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
                // line 18
                echo "                    <tr>
                        <td colspan=\"3\" style=\"font-weight: 900; background-color: #ff614e;\"
                            data-id=\"";
                // line 20
                echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "price_cat_id");
                echo "\">";
                echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "price_cat_name");
                echo "</td>

                    </tr>
                    ";
                // line 23
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["r"]) ? $context["r"] : null), "sub"));
                foreach ($context['_seq'] as $context["_key"] => $context["s"]) {
                    // line 24
                    echo "                        <tr>
                            <td data-id=\"";
                    // line 25
                    echo $this->getAttribute((isset($context["s"]) ? $context["s"] : null), "price_id");
                    echo "\">";
                    echo $this->getAttribute((isset($context["s"]) ? $context["s"] : null), "price_name");
                    echo "</td>
                            <td data-id=\"";
                    // line 26
                    echo $this->getAttribute((isset($context["s"]) ? $context["s"] : null), "price_id");
                    echo "\">";
                    echo $this->getAttribute((isset($context["s"]) ? $context["s"] : null), "unit_name");
                    echo "</td>
                            <td data-id=\"";
                    // line 27
                    echo $this->getAttribute((isset($context["s"]) ? $context["s"] : null), "price_id");
                    echo "\">";
                    echo $this->getAttribute((isset($context["s"]) ? $context["s"] : null), "price_price");
                    echo "</td>

                            ";
                    // line 29
                    if ($this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_id")) {
                        // line 30
                        echo "                                <td>
                                    <a href=\"#\" id=\"edit\"  data-id=\"";
                        // line 31
                        echo $this->getAttribute((isset($context["s"]) ? $context["s"] : null), "price_id");
                        echo "\" class=\"btn-small\">Изменить</a>
                                    <a href=\"#\" id=\"del\"  data-id=\"";
                        // line 32
                        echo $this->getAttribute((isset($context["s"]) ? $context["s"] : null), "price_id");
                        echo "\" class=\"btn-small\">Удалить</a>
                                </td>
                            ";
                    }
                    // line 35
                    echo "                        </tr>
                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['s'], $context['_parent'], $context['loop']);
                $context = array_merge($_parent, array_intersect_key($context, $_parent));
                // line 37
                echo "

                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 40
            echo "                </tbody>

            </table>


        </form>
    </div>
";
        } elseif ((isset($context["rec"]) ? $context["rec"] : null)) {
            // line 48
            echo "    <div class=\"table-responsive\">
        <form id=\"flist\">
            <table class=\"table  table-hover\">
                <thead>
                <tr>
                    <th>Наименование</th>
                    <th>Ед.изм.</th>
                    <th>Цена</th>

                    ";
            // line 57
            if ($this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_id")) {
                // line 58
                echo "                        <th>Действие</th>
                    ";
            }
            // line 60
            echo "                </tr>
                </thead>
                <tbody>

                ";
            // line 64
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["rec"]) ? $context["rec"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
                // line 65
                echo "
                    <tr>
                        <td data-id=\"";
                // line 67
                echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "price_id");
                echo "\">";
                echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "price_name");
                echo "</td>
                        <td data-id=\"";
                // line 68
                echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "price_id");
                echo "\">";
                echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "unit_name");
                echo "</td>
                        <td data-id=\"";
                // line 69
                echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "price_id");
                echo "\">";
                echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "price_price");
                echo "</td>

                        ";
                // line 71
                if ($this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_id")) {
                    // line 72
                    echo "                            <td>
                                <a href=\"#\" id=\"edit\"  data-id=\"";
                    // line 73
                    echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "price_id");
                    echo "\" class=\"btn-small\">Изменить</a>
                                <a href=\"#\" id=\"del\"  data-id=\"";
                    // line 74
                    echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "price_id");
                    echo "\" class=\"btn-small\">Удалить</a>
                            </td>
                        ";
                }
                // line 77
                echo "                    </tr>


                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 81
            echo "                </tbody>

            </table>


        </form>
    </div>
";
        } else {
            // line 89
            echo "    <div class=\"text-center\">
        Нет Контента
    </div>

";
        }
    }

    public function getTemplateName()
    {
        return "desktop/price/page.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  207 => 89,  197 => 81,  188 => 77,  182 => 74,  178 => 73,  175 => 72,  173 => 71,  166 => 69,  160 => 68,  154 => 67,  150 => 65,  146 => 64,  140 => 60,  136 => 58,  134 => 57,  123 => 48,  113 => 40,  105 => 37,  98 => 35,  92 => 32,  88 => 31,  85 => 30,  83 => 29,  76 => 27,  70 => 26,  64 => 25,  61 => 24,  57 => 23,  49 => 20,  45 => 18,  41 => 17,  35 => 13,  31 => 11,  29 => 10,  19 => 2,  17 => 1,);
    }
}
