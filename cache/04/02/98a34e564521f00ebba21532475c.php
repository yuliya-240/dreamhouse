<?php

/* desktop/common/contact.tpl */
class __TwigTemplate_040298a34e564521f00ebba21532475c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"sticky-btns\">
    <form class=\"quick-contact\" method=\"post\" name=\"quick-contact\" style=\"z-index: auto;\">
        <h3>Обратная Связь</h3>
        <div class=\"form-group\">
            <label for=\"qc-name\">Имя</label>
            <input class=\"form-control\" type=\"text\" name=\"qc-name\" id=\"qc-name\" placeholder=\"Введіть свое ім'я\"
                   required>
        </div>
        <div class=\"form-group\">
            <label for=\"qc-email\">Email</label>
            <input class=\"form-control\" type=\"email\" name=\"qc-email\" id=\"qc-email\" placeholder=\"Введіть email\" required>
        </div>
        <div class=\"form-group\">
            <label for=\"qc-tel\">Телефон</label>
            <input class=\"form-control\" type=\"text\" name=\"qc-tel\" id=\"qc-tel\" placeholder=\"Введіть телефон\" required>
        </div>
        <div class=\"form-group\">
            <label for=\"qc-message\">Сообщение</label>
            <textarea class=\"form-control\" name=\"qc-message\" id=\"qc-message\" placeholder=\"Напишіть Ваше повідомлення\"
                      required></textarea>
        </div>
        <button class=\"btn btn-danger btn-block\" id=\"doSendContact\" type=\"submit\" >Отправить</button>
    </form>
    <span id=\"qcf-btn\"><img src=\"/img/004-send.png\"></span>
    <span id=\"scrollTop-btn\"><img src=\"/img/003-up-arrow.png\"></span>
</div><!--Sticky Buttons Close-->
";
    }

    public function getTemplateName()
    {
        return "desktop/common/contact.tpl";
    }

    public function getDebugInfo()
    {
        return array (  17 => 1,);
    }
}
