<?php

/* desktop/common/footer.tpl */
class __TwigTemplate_1a6efb8435d4b132288297f8dbb35801 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<footer class=\"blog-footer\" style=\"text-align: center\" xmlns=\"http://www.w3.org/1999/html\"
        xmlns=\"http://www.w3.org/1999/html\">
    <div class=\"row\">
        <div class=\"col-md-4\">
            <a title=\"Viber\"  href=\"viber://chat?number=+380950556886\"><img src=\"/img/002-viber.png\">
            </a>

            <a title=\"WhatsApp\" href=\"whatsapp://send?phone=+380990805998\"><img src=\"/img/001-whatsapp.png\"></a>
            <a title=\"Facebook\" href=\"https://www.facebook.com/groups/190467651728576/\"><img src=\"/img/003-facebook.png\"></a>

        </div>

        <span class=\"col-md-4\">

\t\t\t<a href=\"tel:+380950556886\">+38 (095) 055-68-86<br></a>
            <a href=\"tel:+380990805998\">+38 (099) 080-59-98</a>
        </span>

        <div class=\"col-md-4\">
            <span class=\"topbar-email\">

\t\t\t\t<a href=\"mailto:dreamhousesumy@gmail.com\">dreamhousesumy@gmail.com</a>
\t\t\t</span>
        </div>
    </div>
</footer>

";
    }

    public function getTemplateName()
    {
        return "desktop/common/footer.tpl";
    }

    public function getDebugInfo()
    {
        return array (  97 => 59,  93 => 57,  91 => 56,  84 => 52,  78 => 49,  72 => 46,  66 => 43,  56 => 35,  28 => 8,  26 => 7,  24 => 6,  22 => 5,  17 => 1,);
    }
}
