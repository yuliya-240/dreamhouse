<?php

/* desktop/common/nav.tpl */
class __TwigTemplate_ab6953b1fbf574d8c7a571e513915190 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<nav class=\"navbar navbar-expand-lg navbar-light bg-light rounded\">
    <div class=\"col-12\">
    <div class=\"row headtoolbar\" style=\"text-align: center\">
        ";
        // line 5
        echo "            ";
        // line 6
        echo "        ";
        // line 7
        echo "        ";
        // line 8
        echo "        <div class=\"col-md-4\" style=\"display:contents\">

            <img src=\"/img/icons/logo2.png\" alt=\"IMG-LOGO\">

        </div>
        <div class=\"col-md-2\">
            <a title=\"Viber\"  href=\"viber://chat?number=+380950556886\"><img src=\"/img/002-viber.png\">
            </a>

            <a title=\"WhatsApp\" href=\"whatsapp://send?phone=+380990805998\"><img src=\"/img/001-whatsapp.png\"></a>
            <a title=\"Facebook\" href=\"https://www.facebook.com/groups/190467651728576/\"><img src=\"/img/003-facebook.png\"></a>


        </div>

\t\t\t\t<span class=\"col-md-3\">
                    +38 (095) 055-68-86<br>
                    +38 (099) 080-59-98
\t\t\t\t</span>

        <div class=\"col-md-3\">
            <p>Напишите нам</p>
\t\t\t\t\t<span class=\"topbar-email\">
\t\t\t\t\t\t<a href=\"mailto:dreamhousesumy@gmail.com\">dreamhousesumy@gmail.com</a>
\t\t\t\t\t</span>

        </div>
    ";
        // line 36
        echo "    </div>
    <div class=\"row\">
    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarsExample10\" aria-controls=\"navbarsExample10\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
        <span class=\"navbar-toggler-icon\"></span>
    </button>

    <div class=\"collapse navbar-collapse justify-content-md-center\" id=\"navbarsExample10\">
        <ul class=\"navbar-nav\">
            <li class=\"nav-item ";
        // line 44
        echo (isset($context["topHome"]) ? $context["topHome"] : null);
        echo "\">
                <a class=\"nav-link\" href=\"/\">Главная</a>
            </li>
            <li class=\"nav-item ";
        // line 47
        echo (isset($context["topPrice"]) ? $context["topPrice"] : null);
        echo "\">
                <a class=\"nav-link\" href=\"/price\">Цены и Услуги</a>
            </li>
            <li class=\"nav-item ";
        // line 50
        echo (isset($context["topGallery"]) ? $context["topGallery"] : null);
        echo "\">
                <a class=\"nav-link\" href=\"/gallery\">Фотогалерея</a>
            </li>
            ";
        // line 54
        echo "                ";
        // line 55
        echo "            ";
        // line 56
        echo "
            ";
        // line 57
        if ($this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_id")) {
            // line 58
            echo "                <li class=\"nav-item\"><a class=\"nav-link\" href=\"/logout\" style=\"color: red;\">Выход</a></li>
            ";
        }
        // line 60
        echo "
        </ul>
    </div>
    </div>
    </div>
</nav>
";
    }

    public function getTemplateName()
    {
        return "desktop/common/nav.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 60,  94 => 58,  92 => 57,  89 => 56,  87 => 55,  85 => 54,  79 => 50,  73 => 47,  67 => 44,  57 => 36,  28 => 8,  26 => 7,  24 => 6,  22 => 5,  17 => 1,);
    }
}
