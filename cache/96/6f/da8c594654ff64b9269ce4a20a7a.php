<?php

/* desktop/common/root.tpl */
class __TwigTemplate_966fda8c594654ff64b9269ce4a20a7a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'headcss' => array($this, 'block_headcss'),
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
            'js' => array($this, 'block_js'),
            'modals' => array($this, 'block_modals'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">
    <meta name=\"description\" content=\"\">
    <meta name=\"author\" content=\"\">

    <title>DreamHouse - Комплексный ремонт квартир и коттеджей</title>

    <link rel=\"apple-touch-icon\" sizes=\"57x57\" href=\"/img/icon/apple-icon-57x57.png\">
    <link rel=\"apple-touch-icon\" sizes=\"60x60\" href=\"/img/icon/apple-icon-60x60.png\">
    <link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"/img/icon/apple-icon-72x72.png\">
    <link rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"/img/icon/apple-icon-76x76.png\">
    <link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"/img/icon/apple-icon-114x114.png\">
    <link rel=\"apple-touch-icon\" sizes=\"120x120\" href=\"/img/icon/apple-icon-120x120.png\">
    <link rel=\"apple-touch-icon\" sizes=\"144x144\" href=\"/img/icon/apple-icon-144x144.png\">
    <link rel=\"apple-touch-icon\" sizes=\"152x152\" href=\"/img/icon/apple-icon-152x152.png\">
    <link rel=\"apple-touch-icon\" sizes=\"180x180\" href=\"/img/icon/apple-icon-180x180.png\">
    <link rel=\"icon\" type=\"image/png\" sizes=\"192x192\"  href=\"/img/icon/android-icon-192x192.png\">
    <link rel=\"icon\" type=\"image/png\" sizes=\"32x32\" href=\"/img/icon/favicon-32x32.png\">
    <link rel=\"icon\" type=\"image/png\" sizes=\"96x96\" href=\"/img/icon/favicon-96x96.png\">
    <link rel=\"icon\" type=\"image/png\" sizes=\"16x16\" href=\"/img/icon/favicon-16x16.png\">
    <link rel=\"manifest\" href=\"/img/icon/manifest.json\">
    <meta name=\"msapplication-TileColor\" content=\"#ffffff\">
    <meta name=\"msapplication-TileImage\" content=\"/img/icon/ms-icon-144x144.png\">
    <meta name=\"theme-color\" content=\"#ffffff\">

    <link rel=\"stylesheet\" type=\"text/css\" href=\"/assets/vendor/select2/css/select2.css\"/>
    <link rel=\"stylesheet\" type=\"text/css\" href=\"assets/vendor/bootstrap/css/bootstrap.css\">
    <link type=\"text/css\" href=\"assets/vendor/sweetalert/sweetalert.min.css\" rel=\"stylesheet\">

    <!-- Custom styles for this template -->
    ";
        // line 35
        echo "    ";
        $this->displayBlock('headcss', $context, $blocks);
        // line 36
        echo "    <link rel=\"stylesheet\" type=\"text/css\" href=\"/css/style.css\">


</head>

<body style='overflow-x:hidden;'>
";
        // line 42
        $this->displayBlock('css', $context, $blocks);
        // line 44
        $template = $this->env->resolveTemplate(((isset($context["skin"]) ? $context["skin"] : null) . "/common/nav.tpl"));
        $template->display($context);
        // line 45
        echo "<main role=\"main\">

";
        // line 47
        $this->displayBlock('content', $context, $blocks);
        // line 52
        echo "</main>


<!-- Footer -->
";
        // line 56
        $template = $this->env->resolveTemplate(((isset($context["skin"]) ? $context["skin"] : null) . "/common/footer.tpl"));
        $template->display($context);
        // line 57
        echo "
";
        // line 58
        $template = $this->env->resolveTemplate(((isset($context["skin"]) ? $context["skin"] : null) . "/common/contact.tpl"));
        $template->display($context);
        // line 59
        echo "

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src=\"/assets/vendor/jquery/jquery-3.2.1.min.js\"></script>
<script src=\"/assets/vendor/jqueryui/jquery-ui.min.js\"></script>
";
        // line 68
        echo "<script src=\"/assets/js/popper.min.js\"></script>
<script src=\"/assets/vendor/select2/js/select2.js\" type=\"text/javascript\"></script>

<script src=\"/assets/vendor/bootstrap/js/bootstrap.min.js\"></script>
<script src=\"/assets/vendor/jquery-validate/jquery.validate.min.js\"></script>
<script src=\"/assets/vendor/sweetalert/sweetalert.min.js\"></script>
<script src=\"/assets/vendor/plupload-2.3.1/js/plupload.full.min.js\"></script>
";
        // line 77
        $this->displayBlock('js', $context, $blocks);
        // line 78
        echo "<script type=\"text/javascript\">
    \$('#qcf-btn').click(function(){
        \$(this).parent().find('.quick-contact').toggleClass('visible');
    });

</script>
<script type=\"text/javascript\">
    if(window.matchMedia('(min-width: 992px)').matches) {
        \$(window).scroll(function () {
            if (\$(this).scrollTop() > 500) {

                \$('#scrollTop-btn').parent().addClass('scrolled');
            } else {
                \$('#scrollTop-btn').parent().removeClass('scrolled');
            }
            if (\$(this).scrollTop() > 112) {

                \$('.navbar').addClass('fixed-top');
                \$('.headtoolbar').hide();
            } else {
                \$('.headtoolbar').show();
                \$('.navbar').removeClass('fixed-top');
            }
        });
        \$('#scrollTop-btn').click(function () {

            \$('html, body').animate({scrollTop: 0}, 300);
        });
    }
</script>
<script type=\"text/javascript\">
    document.ondragstart = noselect;
    document.onselectstart = noselect;
    document.oncontextmenu = noselect;
    function noselect() {return false;}
</script>

";
        // line 115
        $this->displayBlock('modals', $context, $blocks);
        // line 116
        echo "</body>
</html>";
    }

    // line 35
    public function block_headcss($context, array $blocks = array())
    {
    }

    // line 42
    public function block_css($context, array $blocks = array())
    {
    }

    // line 47
    public function block_content($context, array $blocks = array())
    {
        // line 48
        echo "


";
    }

    // line 77
    public function block_js($context, array $blocks = array())
    {
    }

    // line 115
    public function block_modals($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "desktop/common/root.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  185 => 115,  180 => 77,  173 => 48,  170 => 47,  165 => 42,  160 => 35,  155 => 116,  153 => 115,  114 => 78,  112 => 77,  103 => 68,  94 => 59,  91 => 58,  88 => 57,  85 => 56,  79 => 52,  77 => 47,  73 => 45,  70 => 44,  68 => 42,  60 => 36,  57 => 35,  22 => 1,);
    }
}
