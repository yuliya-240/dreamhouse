<?php

/* desktop/dashboard/index.tpl */
class __TwigTemplate_c5f81dc66655c5d9df60926d67484a91 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'footer' => array($this, 'block_footer'),
            'js' => array($this, 'block_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate(((isset($context["skin"]) ? $context["skin"] : null) . "/common/root.tpl"));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class=\"jumbotron\" style='background-image: url(\"/img/img.jpeg\"); background-size: cover'>
        <div class=\"container\">
            <h1 class=\"display-3\" style=\"text-align: center\">Комплексный Ремонт Квартир и Коттеджей</h1>
        </div>
    </div>


    <div class=\"container\">
        <h5 style=\"text-align: center;\"><span style=\"color: #000000;\">В наше время комплексный ремонт квартиры можно доверить только команде профессионалов,<br/>и в этом мы Вам поможем!</span>
        </h5>
        <!-- Example row of columns -->
        <div class=\"row\">
            <div class=\"col-md-3\" style=\"text-align: center\">
                <h6 style=\"font-width: 900;\">Консультации</h6>
                <img src=\"/img/main/i.jpg\" alt=\"i\" width=\"127\" height=\"204\"/>

                <p>Вы получите всю необходимую информацию о ремонте. Для заказчика мы предоставляем полный спектр
                    <strong>ремонтно-строительных услуг:</strong> консультация и помощь в дизайне помещений,
                    реконструкцию и ремонт,
                    монтажные работы, работы по утеплению и реставрации фасадов, сантехнические, электромонтажные а
                    также отделочные работы и декорирование.</p>
            </div>
            <div class=\"col-md-3\" style=\"text-align: center\">
                <h6 style=\"font-width: 900;\">Профессионализм</h6>
                <img src=\"/img/main/367-240x300.jpg\" alt=\"367\" width=\"166\" height=\"208\"/>

                <p>Наш опыт в <strong>строительной сфере свыше 10-ти лет</strong> и нам доверяют многие. <strong>Комплексный
                        ремонт квартир</strong> требует:
                    организованности, опыта работы, применение качественных материалов, гарантию и безопасность.</p>
            </div>
            <div class=\"col-md-3\" style=\"text-align: center\">
                <h6 style=\"font-width: 900;\">Опытные мастера</h6>
                <img src=\"/img/main/607422630-300x300.jpg\" alt=\"\" width=\"196\" height=\"196\"/>

                <p>Мы проводим <strong>высококачественный ремонт и отделку домов, коттеджей, квартир, дач в
                        Сумах</strong> оптимально
                    используя все возможности наших сотрудников: опыт, вкус, мастерство. В нашей бригаде работают
                    мастера
                    разной специализации, но каждый из них является профессионаломв своей сфере.</p>
            </div>
            <div class=\"col-md-3\" style=\"text-align: center\">
                <h6 style=\"font-width: 900;\"><strong>ГАРАНТИЯ КАЧЕСТВА</strong></h6>
                <img src=\"/img/main/M_OK-300x300.jpg\" alt=\"3d human with big positive symbol\" width=\"194\" height=\"194\"/>

                <p><strong>Мы гарантируем качество и надежность, у нас большой опыт работ и выполнение сложных объектов.
                        Мы знаем
                        множество стандартных и индивидуальных перепланировок. Все работы проводятся в установленные
                        сроки.</strong></p>
            </div>

        </div>

        <hr>

    </div> <!-- /container -->

    <div class=\"container\">
        <div class=\"row\">

            <div class=\"col-sm-12\">

                <h5 style=\"color: #000000; text-align: center\">Сроки выполнения работ</h5>
                <h6 style=\"text-align: justify\">От чего зависит время выполнения ремонта? Конечно же, от квадратных
                    метров, которые нужно
                    отремонтировать, от того, в каком состоянии, эти квадратные метры, нужен ил демонтаж, и от сложности
                    работ,
                    которые нужно выполнять на этих квадратных метрах.
                    Мы даем стандартные сроки на выполнение ремонтных работ. Но помните, что даже одинаковые работы
                    в соседних квартирах могут быть выполнены за разное время.</h6>
                <hr>
            </div>

            <div class=\"col-sm-12\" style=\"text-align: center\"><h3><strong>Сроки ремонта квартир</strong></h3></div>
            <div class=\"col-12\">
                <table class=\"table  table-hover\" width=\"100%\">
                    <tbody>
                    <tr>
                        <td></td>
                        <td>
                            <strong>1-к квартира</strong>
                        </td>
                        <td>
                            <strong>2-к квартира</strong>
                        </td>
                        <td>
                            <strong>3-к квартира</strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Косметический ремонт</strong>
                        </td>
                        <td>
                            12-20 дней
                        </td>
                        <td>
                            18-35 дней
                        </td>
                        <td>
                            30-45 дней
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Комплексный ремонт</strong>
                        </td>
                        <td>
                            25-35 дней
                        </td>
                        <td>
                            45-60 дней
                        </td>
                        <td>
                            60-90 дней
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Элитный ремонт</strong>
                        </td>
                        <td>
                            30-45 дней
                        </td>
                        <td>
                            60-70 дней
                        </td>
                        <td>
                            70-100 дней
                        </td>
                    </tr>
                    </tbody>
                </table>

                <hr>
            </div>
            <div class=\"col-sm-12\" style=\"text-align: center\"><h3><strong>Виды ремонта</strong></h3></div>

            <div class=\"container\" style=\"text-align: center\">
                <div id=\"accordion\">


                    <div class=\"card\">
                        <div class=\"card-header\" id=\"headingOne\">
                            <h2><strong>Косметический ремонт</strong></h2>
                        </div>
                    </div>
                    <div id=\"collapseOne\" class=\"collapse show\" aria-labelledby=\"headingOne\" data-parent=\"#accordion\">
                        <div class=\"card-body\">
                            <p>
                                <strong>Косметический ремонт </strong>является самым дешевым и простым видом. Его
                                проводят в том случае, если ваш дом не нуждается в
                                особых переделках, а просто пришло время обновить стены и потолок, внести новые краски в
                                привычную
                                обстановку.
                            </p>

                        </div>
                    </div>


                    <div class=\"card\">
                        <div class=\"card-header\" id=\"headingOne\">

                            <h2><strong>Комплексный ремонт </strong></h2>
                        </div>
                    </div>
                    <div id=\"collapseOne\" class=\"collapse show\" aria-labelledby=\"headingOne\" data-parent=\"#accordion\">
                        <div class=\"card-body\">
                            <p><strong>Комплексный ремонт </strong>
                                коттеджей, домов под ключ является более сложным видом ремонта - это
                                отделка помещения с учетом современных тенденций. Осуществляется частичный или полный
                                демонтаж стен,
                                замена потолков, полов и инженерных линий.
                            </p>

                        </div>
                    </div>

                    <div class=\"card\">
                        <div class=\"card-header\" id=\"headingOne\">
                            <h2><strong>Элитный ремонт</strong></h2>
                        </div>
                    </div>
                    <div id=\"collapseOne\" class=\"collapse show\" aria-labelledby=\"headingOne\" data-parent=\"#accordion\">
                        <div class=\"card-body\">
                            <p><strong>Элитный ремонт </strong>
                                Наиболее дорогой и качественный вид ремонта. Включает использование исключительно
                                дорогих отделочных
                                материалов и применения новейших строительных технологий.
                            </p>

                        </div>
                    </div>

                </div>
                <hr>
            </div>

            <div class=\"container\">
                <h3 style=\"text-align: center;\">Этапы проведения комплексного ремонта</h3>

            </div>
            <div class=\"row\">

                <div class=\"col-md-3\" style=\"text-align: center\">
                    <img src=\"/img/main/etap1.png\" alt=\"etap1\" width=\"120\" height=\"125\"/>
                    <h4><strong>Этап 1</strong></h4>
                    <h5><strong>Составление и утверждение сметы.
                            Заключение договора</strong></h5>
                </div>
                <div class=\"col-md-3\" style=\"text-align: center\">
                    <img src=\"/img/main/etap2.png\" alt=\"etap2\" width=\"120\" height=\"125\"/>
                    <h4><strong>Этап 2 </strong></h4>
                    <h5><strong>Перепланировка квартиры,
                            демонтажные работы, монтаж перегородок</strong></h5>
                </div>
                <div class=\"col-md-3\" style=\"text-align: center\">
                    <img src=\"/img/main/etap3.png\" alt=\"etap3\" width=\"120\" height=\"125\"/>
                    <h4><strong>Этап 3</strong></h4>
                    <h5><strong>Штробление стен под
                            прокладку различных кабелей и труб
                            штукатурка стен, гипсокартонные работы, стяжка пола</strong></h5>
                </div>
                <div class=\"col-md-3\" style=\"text-align: center\">
                    <img src=\"/img/main/etap4.png\" alt=\"etap4\" width=\"120\" height=\"125\"/>
                    <h4><strong>Этап 4 </strong></h4>
                    <h5><strong>Финишная шпатлёвка стен и потолков.
                            Установка розеток и выключателей</strong></h5>
                </div>
                <div class=\"col-md-4\" style=\"text-align: center\">
                    <img src=\"/img/main/etap5.png\" alt=\"etap5\" width=\"120\" height=\"125\"/>
                    <h4><strong>Этап 5</strong></h4>
                    <h5><strong>Поклейка обоев, покраска стен и потолков,
                            укладка напольных покрытий
                            (плитка, паркет, ламинат и т.д.)
                            установка плинтуса</strong></h5>
                </div>
                <div class=\"col-md-4\" style=\"text-align: center\">
                    <img src=\"/img/main/etap6.png\" alt=\"etap6\" width=\"120\" height=\"125\"/>
                    <h4><strong>Этап 6</strong></h4>
                    <h5><strong>Установка унитаза, ванной,
                            душевой кабины, смесителей,
                            установка дверей, монтаж карнизов,
                            потолочных и настенных светильников.</strong></h5>
                    &nbsp;
                </div>
                <div class=\"col-md-4\" style=\"text-align: center\">
                    <img src=\"/img/main/etap7.png\" alt=\"etap7\" width=\"120\" height=\"125\"/>

                    <h1><strong>Этап 7</strong></h1>

                    <h2><strong>Сдача ремонта заказчику</strong></h2>


                </div>


                <hr>
            </div>
            <div class=\"row\">
                <h3 style=\"text-align: center;\">Закажите выезд специалиста по телефону :<a href=\"tel:+380950556886\">+38
                        (095) 055-68-86</a> или
                    <a href=\"tel:+380990805998\">+38 (099) 080-59-98</a></h3>

                <h3 style=\"text-align: center;\"><strong>Выезд специалиста
                        <span style=\"color: #ff1a13;\">БЕСПЛАТНО!</span> </strong><strong>Составление сметы</span>
                    </strong><strong><span style=\"color: #ff1a13;\">БЕСПЛАТНО!</span> </strong><strong>Цены на
                        материалы</span>
                    </strong><strong>на 10% ниже</strong><strong> <span
                                style=\"color: #0000ff;\">\"Эпицентра\"</span> </strong></h3>
                <h6 style=\"text-align: center;\">Наш мастер приедет в удобное для вас время и выполнит замеры для
                    составления сметы.
                    А также проконсультирует вас по строй материалам необходимым для ремонта квартиры</h6>

                <h3 style=\"text-align: center;\">Мы сделаем все правильно, качественно и в соответствии строительным
                    нормам.</h3>


            </div>


        </div>
        <hr>

    </div>





";
    }

    // line 296
    public function block_footer($context, array $blocks = array())
    {
        // line 297
        echo "    <div id=\"megafooter\"></div>

";
    }

    // line 302
    public function block_js($context, array $blocks = array())
    {
        // line 303
        echo "    <script src=\"/js/main.js\" type=\"text/javascript\"></script>

";
    }

    public function getTemplateName()
    {
        return "desktop/dashboard/index.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  336 => 303,  333 => 302,  327 => 297,  324 => 296,  29 => 4,  26 => 3,);
    }
}
