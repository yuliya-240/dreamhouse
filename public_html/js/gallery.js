var $container = $('.gallery-container');
var mas;


function aplyMassonry() {

    //$container.masonry().masonry('destroy');
    $container.masonry({
        columnWidth: 0,
        itemSelector: '.item'
    });



}

function getContent(p) {
    $.get('/gallery/getContent', {p: p}, function (r) {

        $("#d-content").html(r.html);

        if (r.pag.next > 0) {
            $("#load-more").data("next", r.pag.next).show();
        }

        $('#d-content').imagesLoaded( function() {
            aplyMassonry();

        });
        $("#megafooter").html(r.footer);


    }, "json");
}

$(document).ready(function () {

    var ss = $('#s').val();
    getContent(ss);


    $('#s').keyup(function (e) {

        if (e.which == 13) {
            e.preventDefault();
        }

        var searchstr = $(this).val();

        if(searchstr.length){

            $("#delsearch").show();
        }else{
            $("#delsearch").hide();
        }


        getContent(searchstr);

        $.get('/gallery/getContent', {query: searchstr}, function (r) {

            $("#d-content").html(r.html);
            aplyMassonry()

        }, "json");


    });


});

//$(document).on("click", ".item", function (e) {
//    e.preventDefault();
//    showProcess();
//    var id = $(this).data("id");
//    $.get('/gallery/view', {id: id}, function (r) {
//        endProcess();
//
//    }, "json");
//
//    return false;
//});

function appendContent(p) {
    $.get('/gallery/getContent', {p: p}, function (rez) {
        var $boxHtm = $(rez.html);
        $container.append($boxHtm);
        $('#d-content').imagesLoaded( function() {
            //$container.masonry('reloadItems')
            $container.masonry('appended', $boxHtm).masonry('layout');

        });
        if (rez.pag.next > 0) {
            $("#load-more").data("next", rez.pag.next).show();
        } else {

            $("#load-more").hide();
        }
    }, "json")
}
$(document).on("click", "#load-more", function () {
    var p = $(this).data("next");
    console.log(p);
    appendContent(p);
    //aplyMassonry();
    return false;
});
$(document).on("click",".trash", function(){
    var id = $(this).data("id");
    var msg = $("#_gallerydelmes").val();
    var title = $("#_gallerydeltittle").val();
    var yes = $("#_yes").val();

    swal({

        text: msg,
        title:title,
        type: "warning",
        confirmButtonText: yes,
        confirmButtonColor: "#DD6B55",
        closeOnConfirm: true,
        showCancelButton: true,

    }).then( function (result) {
        if (result) {
            //showProcess();
            $.post("/gallery/del", {id: id}, function () {
                //endProcess();
                $(".div" + id).remove();
                $container.masonry('reloadItems');
                $container.masonry("layout");
            }, "json");
        }
    })
    return false;

});
$(document).on("click",".edit", function(){
    var id = $(this).data("id");
    window.location.href = '/gallery/edit/' + id;
});
