function setCookie(name, value) {
    var curCookie = name + "=" + escape(value)

        document.cookie = curCookie

}
$(document).on("click", ".lang", function(){
    var lang=$(this).val();
    setCookie("lang",lang);
    location.reload();
    return false;
});

function getContent(searchstr){

    $.get('/expenses/getContent',{query:searchstr},function(r){

        $("#d-content").html(r.html);

        var c = parseInt(r.count);
        if(c>0){

            $("#megafooter").html(r.footer);
        }

        endProcess();
    },"json");
}

$(document).ready(function() {


    var ss = $('#s').val();
    getContent(ss);

    $('#s').keyup(function (e) {

        if (e.which == 13) {
            e.preventDefault();
        }

        var searchstr = $(this).val();

        if (searchstr.length) {

            $("#delsearch").show();
        } else {
            $("#delsearch").hide();
        }


        getContent(searchstr);
    });
});