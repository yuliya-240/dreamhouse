var uploader;

var $container = $('#d-content');

var uploader2;

function aplyMassonry() {
    $container.masonry("destroy");
    $container.masonry({
        columnWidth: 0,
        itemSelector: '.item'
    });
}

function doFormValidation() {
    var validator = $("#fdata").validate({
        errorPlacement: function (label, elem) {
            var errbox = elem.data("errorbox");
            $(errbox).html(label);
            window.location.href="#";
        }
    });
    return validator.form();
}

uploader2 = new plupload.Uploader({
    runtimes : 'html5,flash,html4',
    browse_button : 'pickfiles2', // you can pass an id...
    container: document.getElementById('counteinerpic2'), // ... or DOM Element itself
    url : '/img/uploadgalleryall',
    unique_names:true,
    multi_selection:true,
    max_file_count: 20,



    filters : {
        max_file_size : '10mb',
        mime_types: [
            {title : "Image files", extensions : "jpg,gif,png,jpeg"},

        ]
    },

    init: {
        PostInit: function() {},

        FilesAdded: function(up, files) {
            $("#progress2").show();
            $("#progressSlide2").css('width',"0%");
            up.refresh(); // Reposition Flash/Silverlight
            uploader2.start();

            //showProcess();
        },

        UploadProgress: function(up, file) {
            $("#progressSlide2").css('width',file.percent+"%");
            $("#progress2").data('percent',file.percent+"%");

        },

        Error: function(up, err) {

            swal({
                title:err.message,
                text:err.code,
                type:"error"
            })

            up.refresh(); // Reposition Flash/Silverlight

            $("#progress2").hide();



        },

        FileUploaded: function(up, file, info) {

            var obj = JSON.parse(info.response);
            var $htm = $(obj.html);
            $container.append($htm);
            $container.imagesLoaded(function(){
                $container.masonry('reloadItems');
                $container.masonry("layout");
                $('.image-zoom2').venobox();
            });

        },
        UploadComplete: function(up,files){
            $("#progress2").hide();
            //endProcess();

        }
        }
});

$(document).ready(function () {
    $container.imagesLoaded(function(){
        aplyMassonry();
    });
    $('#notes').redactor({

        buttonSource: true,
        minHeight: 200

    });

    uploader = new plupload.Uploader({
        runtimes : 'html5,flash,html4',
        browse_button : 'pickfiles', // you can pass an id...
        container: document.getElementById('counteinerpic'), // ... or DOM Element itself
        url : '/img/uploadgallerypic',
        unique_names:true,
        multi_selection:false,
        max_file_count: 1,


        filters : {
            max_file_size : '10mb',
            mime_types: [
                {title : "Image files", extensions : "jpg,gif,png,jpeg"},

            ]
        },

        init: {
            PostInit: function() {
                var pic = $("#u-ava").prop("src");
                $("#target").prop("src",pic);
                var fcover = $("#fcover").val();
                if(fcover=="nopic.jpg"){
                    $("#del-pic").hide();
                }else{
                    $("#del-pic").show();
                }

            },

            FilesAdded: function(up, files) {
                $("#progressSlide").css('width',"0%");
                $("#progress1").show();
                $("#btns").hide();
                up.refresh(); // Reposition Flash/Silverlight
                uploader.start();
            },

            UploadProgress: function(up, file) {
                $("#progressSlide").css('width',file.percent+"%");
                $("#progress1").data('percent',file.percent+"%");
            },

            Error: function(up, err) {

                swal({
                    title:err.message,
                    text:err.code,
                    type:"error"
                })

                up.refresh(); // Reposition Flash/Silverlight

                $("#progress1").hide();
                $("#btns").show();


            },

            FileUploaded: function(up, file, info) {
                //alert(info);
                $("#progress1").hide();
                var obj = JSON.parse(info.response);
                if(!obj.status){
                    swal({
                        title:obj.title,
                        text:obj.msg,
                        type:"error"
                    })

                }else{
                    $('#target').prop('src',obj.imghost+obj.cid+'/pic/'+obj.cleanFileName);

                    $("#fname").val(obj.cleanFileName);

                    $("#ftype").val(obj.type);
                    $("#fwidth").val(obj.width);
                    $("#fheight").val(obj.height);

                    $("#md-custom").find('.modal-content').css({'max-width':obj.width });

                    $("#picSave").show();
                    $("#del-pic").show();

                }
                $("#btns").show();

            }
        }
    },"json");

    uploader.init();

    uploader2.init();

    //$('#md-custom').on('shown.bs.modal', function () {
    //
	 //   var fcover = $("#fcover").val();
	 //   if(fcover=="nopic.jpg"){
		//    $("#del-pic").hide();
	 //   }
    //    uploader.refresh();
    //
    //});

});

$(document).on("click","#btnCancel",function(){

    $("#md-custom").modal('hide');
    var pic = $("#u-ava").prop("src");
    $("#target").prop("src",pic);
    $("#picSave").hide();
    return false;
});

$(document).on("click","#picSave",function(){

    var  pd = $("#fcoord").serialize();

    $.post('/img/creategallerypic',pd,function(r){

        $("#md-custom").modal('hide');
        $("#picSave").hide();
        $("#cover-pic").html("<img src=/userfiles/"+ r.cid +"/pic/"+ r.fname +">");
        $("#fcover").val(r.fname);
        $("#target").prop("src","/userfiles/"+ r.cid +"/pic/"+ r.fname );

    },"json");

    return false;
});

$(document).on("click", "#del-pic", function(){
    $("#fname").val('nopic.jpg');
    $("#fcover").val("nopic.jpg");
    $("#cover-pic").html('<img src="/img/nopic.jpg">');
    $("#target").prop("src","/img/nopic.jpg");
    $("#picSave").hide();
    $("#del-pic").hide();
    return false;
});

$(document).on("click", "#doSave", function () {

    var isvalid = doFormValidation();

    if (isvalid) {
        var pd = $("#fdata").serialize();
        $.post('/gallery/doAdd', pd, function (r) {

            window.location.href = "/gallery";

        }, "json");
    }

    return false;
});

$(document).on("click",".s7-trash", function(){
    var id = $(this).data("id");
    $("#item"+id).remove();
    $("#inp_"+id).remove();
    $container.masonry('reloadItems');
    $container.masonry("layout");
 
});