function getContent(p) {
    $.get('/price/getContent', {p: p}, function (r) {
        $("#d-content").html(r.html);
        $("#megafooter").html(r.footer);
    }, "json");
}

$(document).ready(function () {
    $(".select2").select2({
        width: '100%',

    });

    var ss = $('#s').val();
    getContent(ss);

    $('#s').keyup(function (e) {
        if (e.which == 13) {
            e.preventDefault();
        }
        var searchstr = $(this).val();
        if(searchstr.length){
            $("#delsearch").show();
        }else{
            $("#delsearch").hide();
        }
        getContent(searchstr);
        $.get('/price/getContent', {query: searchstr}, function (r) {
            $("#d-content").html(r.html);
        }, "json");
    });

});

$(document).on("change","#cat",function(){
    var cat = $(this).val();
    $.post('/price/changeOptions',{cat:cat},function(r){
        getContent("");
    },"json");
});

$(document).on("click", "#edit",function(){
    var id = $(this).data("id");
    $.get("/price/editCat", {id: id}, function(r){
        $(".edit-cat").html(r.html);
        $('#new-cat-box').modal('show');
        $("#doSave").hide();
        $("#doEdit").show();

    },"json");
    return false;
});

$(document).on('click', '#doEdit', function(){
    var pd = $('#fdata').serialize();
    $.post("/price/doEditCat",pd, function(r){
        $('#new-cat-box').modal('hide');
            getContent();
    },"json");
});

$(document).on("click","#del", function(){
    var id = $(this).data("id");

            $.post("/price/delete", {id: id}, function () {

                getContent();
            }, "json");

    return false;

});
$(document).on("click", "#add",function(){
    $.get("/price/add", function(r){
        $(".edit-cat").html(r.html);
        $('#new-cat-box').modal('show');
        $("#doSave").show();
        $("#doEdit").hide();
    },"json");
    return false;
});
$(document).on('click', '#doSave', function(){
    var pd = $('#fdata').serialize();
    $.post("/price/doAdd",pd, function(r){
        $('#new-cat-box').modal('hide');
        getContent();
    },"json");
});